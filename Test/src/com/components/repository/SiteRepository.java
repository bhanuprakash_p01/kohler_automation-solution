/**
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 *																																		   	*
 * 2011-2012 Infosys Limited, Banglore, India. All Rights Reserved																			*
 * Version: 2.0																																*
 * 																																			*
 * Except for any free or open source software components embedded in this Infosys proprietary software program ("Program"),				*
 * this Program is protected by copyright laws, international treaties and other pending or existing intellectual property rights in India, *
 * the United States and other countries. Except as expressly permitted, any unautorized reproduction, storage, transmission 				*
 * in any form or by any means (including without limitation electronic, mechanical, printing, photocopying, recording or otherwise), 		*
 * or any distribution of this Program, or any portion of it, may result in severe civil and criminal penalties, 							*
 * and will be prosecuted to the maximum extent possible under the law 																		*
 *																																			*
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 **/
package com.components.repository;


import com.components.pages.Kohler_DTVPage;
import com.components.pages.Kohler_GeneralNavigation;
import com.components.pages.Kohler_HomePage;
import com.components.pages.Kohler_ProductDisplayPage;
import com.components.pages.Kohler_SearchPage;


public class SiteRepository{

	
	public Kohler_HomePage homePage()
	
	{
		return new Kohler_HomePage(this);
	}
	
		
	public Kohler_GeneralNavigation generalNavigationPage()
	
	{
		return new Kohler_GeneralNavigation(this);
	}
	public Kohler_ProductDisplayPage KohlerPDPPage()
    
    {
           return new Kohler_ProductDisplayPage(this);
    }
	
	public Kohler_DTVPage dtvPage()
	{
		return new Kohler_DTVPage(this);
	}
	
	public Kohler_SearchPage searchResultPage()
	
	{
		return new Kohler_SearchPage(this);
	}
	
	

}