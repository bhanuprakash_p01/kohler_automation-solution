package com.components.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.components.repository.SiteRepository;
import com.components.yaml.LoginData;
import com.components.yaml.SearchData;
import com.iwaf.framework.components.Target;
import com.iwaf.framework.components.IReporter.LogType;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Kohler_ProductDisplayPage extends SitePage
{
/* Defining the locators on the Page */ 
	
	public static final Target Search_HomePage  = new Target("Search_HomePage","//*[@id='nav-searchbox']",Target.XPATH);
	
	public static final Target FindaStore  = new Target("FindaStore","//*[@id='storeLocator']/button",Target.XPATH);
	
	public static final Target Searchbtn_HomePage  = new Target("Searchbtn_HomePage","//*[@id='header__button--search-button-desktop']/i",Target.XPATH);
	
	public static final Target PDP_Thubnailsdownarrow  = new Target("PDP_Thubnailsdownarrow","//*[@id='js-productDetail-imageSwitcherView']/div[3]/div/button",Target.XPATH);
	
	public static final Target StoreLocator_PDPPage  = new Target("StoreLocator_PDPPage","//div[@id='product-detail__add_to_cart_wrapper']//following::button[3]",Target.XPATH);
	
	public static final Target StoreLocatorHeader  = new Target("StoreLocatorHeader","//*[@id='store-locator']/div/h1",Target.XPATH);
	
	public static final Target CompareButton  = new Target("StoreLocatorHeader","//*[@id='product-detail__compare_wrapper']/div[3]/button",Target.XPATH);
	
	public static final Target CompareOverlay  = new Target("CompareOverlay","//*[@id='compare-products-bar__dropdown-button']/span[1]",Target.XPATH);
	
	public static final Target CompareOverlayProductdetails  = new Target("CompareOverlayProductdetails","//*[@id='compare-product--1']/div/p[2]",Target.XPATH);
	
	public static final Target CompareOverlay_ClearAll  = new Target("CompareOverlay_ClearAll","//*[@id='compare-products-bar__clear-all-button']",Target.XPATH);
	
	public static final Target CompareOverlay_Expand  = new Target("CompareOverlay_Expand","//*[@id='compare-products-bar__dropdown-button']/span[2]/i",Target.XPATH);

	public static final Target PDP_AddToCart  = new Target("PDP_AddToCart","//*[@id='productDetails']/div[3]/button[1]",Target.XPATH);	
	
	public static final Target PDP_AddToCartModal  = new Target("PDP_AddToCartModal","//*[@id='modal--added-to-cart']/div",Target.XPATH);
	
	public static final Target PDP_AddToCartModalHeader  = new Target("PDP_AddToCartModalHeader","//*[@id='modal--added-to-cart']/div/div[1]/div[2]/h5",Target.XPATH);
		
	public static final Target PDP_AddToCartContinueShopping  = new Target("PDP_AddToCartContinueShopping","//*[@id='added-to-cart']/div/div/footer/div/button",Target.XPATH);
		
	public static final Target PDP_CartIconFlag  = new Target("PDP_CartIconFlag","//*[@id='main-nav__item--cart']/a/span/span",Target.XPATH);
	
	public static final Target PDP_CartIcon = new Target("PDP_CartIcon","//*[@id='main-nav__item--cart']/a/span",Target.XPATH);
	
	public static final Target CartPage_Header = new Target("CartPage_Header","//*[@id='shopping-cart']/div[1]/div/h1",Target.XPATH);
		
	public static final Target Productdetail_CartPage = new Target("Productdetail_CartPage","//*[@id='shopping-cart']/div[2]/div[2]/div[2]/div[1]/div[2]/p[1]/a",Target.XPATH);
	
	public static final Target ContinueShopping_CartPage = new Target("ContinueShopping_CartPage","//*[@id='cart-continue-shopping-button']",Target.XPATH);
	
	public static final Target PairsWell = new Target("PairsWell","//*[@id='product-detail__pairs-well-with']/h2",Target.XPATH);
	
	public static final Target AddRequiredItems = new Target("AddRequiredItems","//*[@id='product-detail__add-required-items-button'][1]",Target.XPATH);
	
	public static final Target RequiredItemsHeader = new Target("RequiredItemsHeader","//*[@id='modal--required-items']/div/h4",Target.XPATH);
	
	public static final Target SubTotal = new Target("SubTotal","//*[@id='product-detail__required-items-sub-total']/span[2]",Target.XPATH);
		
	public static final Target DiscontinuedCrousel = new Target("DiscontinuedCrousel","//*[@id='product-detail__carousel-hero-slides']/div/div/div/div[1]/div",Target.XPATH);
	
	public static final Target Removeproduct = new Target("Removeproduct","//*[@class='order-detail-table__product-action']",Target.XPATH);

	/* Defining the locators for Comparing products */
	
	public static final Target Kitchens = new Target("Kitchens","//*[@id='main-nav__button--kitchen']",Target.XPATH);
	
	public static final Target Accessories = new Target("Accessories","//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]/ul/li[3]/a",Target.XPATH);
	
	public static final Target ProductMaterial = new Target("ProductMaterial","//*[@id='article-page__article-panels']/div/div[1]/div[1]/a/img[1]",Target.XPATH);
	
	public static final Target Compare_btn = new Target("Compare_btn","//*[@id='compare-products-bar__dropdown-button-container']/a",Target.XPATH);
	
	public static final Target CompareModal_Header = new Target("CompareModal_Header","//*[@id='modal--compare-products']/div/h2",Target.XPATH);

	/* Defining the locators for SignIn */
	public static final Target btn_SigninHomePage  = new Target("btn_SigninHomePage","//*[@id='sign-in-bar__inner']/button[2]",Target.XPATH);
	
	public static final Target SignInModalPopup  = new Target("SignInModalPopup","//*[@id='modal--sign-in']/div",Target.XPATH);
	
	public static final Target EmailAddress  = new Target("EmailAddress","//*[@id='traySignInForm']/fieldset/div/input[@id='email']",Target.XPATH);
	
	public static final Target Password  = new Target("Password","//*[@id='traySignInForm']/fieldset/div/input[@id='password']",Target.XPATH);
	
	public static final Target btn_Signin  = new Target("btn_Signin","trayProfileSignIn",Target.ID);
	
	public static final Target Discontinued  = new Target("Discontinued","//*[@id='product-detail__features']/div[2]/p[1]/a",Target.XPATH);

	
	//Defining the locators for Edge Browser	
	public static final Target Product  = new Target("Product",".//*[@id='product-category__product-panels']/div/div/a/div/img",Target.XPATH);
	
	public static final Target ProductGrid  = new Target("ProductGrid","//*[@id='product-category__product-panels']/div[1]/div/a[1]/div/img",Target.XPATH);
	
	public static final Target DisContinuedProductSearch = new Target("DisContinuedProductSearch",".//*[@id='search-hero-navigation']/a",Target.XPATH);
	
	public static final Target Sellablapart = new Target("Sellablapart",".//div[@id='search-hero-navigation']/a",Target.XPATH);
	
	public static final Target CompareOverlayArrow = new Target("CompareOverlayArrow","",Target.XPATH);

	public static final Target SignUpForEmail = new Target("SignUpForEmail","(//*[@id='signupModal']//following::div[@class='main-content-email-pop']/span[1])",Target.XPATH);
		
	public static final Target SignUpForEmailClose = new Target("SignUpForEmailClose","(//*[@id='signupModal']//following-sibling::div[@class='modal-content']/span[1])",Target.XPATH);
		
	public static final Target KohlerLogo = new Target("KohlerLogo",("//img[@id='main-nav__logo']"),Target.XPATH);
	
	JavascriptExecutor js = (JavascriptExecutor) getCommand().getDriver(); 
	
	//For getting Browser Name
	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();

	public Kohler_ProductDisplayPage(SiteRepository repository)
	{
		super(repository);
	}
	
	// SignUp pop-up on Home Page
	 	public Kohler_ProductDisplayPage SignUpPopUp()
	 	{
	 		boolean flag = getCommand().isTargetVisible(KohlerLogo);
	 		if(flag)
	 		{
	 			Assert.assertTrue(flag, "Unable to view the Kohler Logo");
	 		}
	 		else
	 		{
	 			String value = getCommand().getText(SignUpForEmail);			
	 	 		if(value != null)
	 	 			log("Alert is not present on HomePage",LogType.STEP);
	 	 		else
	 	 			log("Alert is present on home page",LogType.STEP);
	 	 			getCommand().click(SignUpForEmailClose);
	 	 			
	 		}
	 		
	 			return this;
	 	}
	 	

	/* Functions on the Page are defined below */
	
	public Kohler_ProductDisplayPage atkohlerproductdisplaypage()
	{
		log("Launched Kohler Site",LogType.STEP);
		//getCommand().captureScreenshot("C:\\Users\\Arvind01\\Desktop\\Add To Cart\\HomePage.png");
		return this;
	}
	
	//Method to verify PDPpage
	public Kohler_ProductDisplayPage VerifyPDPpage(String Data)
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			
			String ProductID = search.ProductSku;
			String ExpectedTitle = search.ExpectedTitle;
			
			Search(ProductID);
			
			String browserName = caps.getBrowserName();
			
			if(browserName.equals("MicrosoftEdge"))
			{
				getCommand().waitForTargetVisible(Product, 120);
				getCommand().isTargetPresent(Product);
				getCommand().click(Product);
			}
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver, 30);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("main-nav__logo")));
			
			String ActualTitle = getCommand().getPageTitle();
			
			String SkuDetails = "K-"+ProductID;
			
			String SkuFromTitle = ActualTitle.substring(0, 9);
			log("Verifying The Sku/product number in the page title is feature as (K-14660-4 or K-14660-4-CP)",LogType.STEP);
			Assert.assertEquals(SkuDetails, SkuFromTitle, "The Sku/product number in the page title is not feature as (K-14660-4 or K-14660-4-CP). Expected: "+ SkuDetails+ ", Actula: "+SkuFromTitle);
			log("Verifying page title",LogType.STEP);
			Assert.assertEquals(ActualTitle, ExpectedTitle, "Mismatch in page title. Expected: "+ ExpectedTitle+ ", Actual: "+ActualTitle);
			
			log("Page title is as expected",LogType.STEP);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	//Method to verify Breadcrumbs_StoreLocator_PDPpage
	public Kohler_ProductDisplayPage VerifyBreadcrumbs_StoreLocator_PDPpage(String Data)
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			
		    String ProductID = search.ProductSku;
			Search(ProductID);
			
            String browserName = caps.getBrowserName();
			
			if(browserName.equals("MicrosoftEdge"))
			{
				getCommand().waitForTargetVisible(Product, 120);
				getCommand().isTargetPresent(Product);
				getCommand().click(Product);
			}
			
			getCommand().waitForTargetVisible(PDP_AddToCart, 100);
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
			List<WebElement> BreadCrumbs = getCommand().driver.findElements(By.xpath("//*[@id='breadcrumb-navigation']/div/a"));
			String pageTitle = getCommand().getPageTitle();
			for(WebElement BreadCrumb : BreadCrumbs)
			{
				String BreadCrumbText = BreadCrumb.getText();
				BreadCrumbText = BreadCrumbText.substring(0, BreadCrumbText.length() - 2);
				String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
				getCommand().waitFor(2);
				BreadCrumb.sendKeys(selectLinkOpeninNewTab);
				//WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
				wait.until(ExpectedConditions.numberOfwindowsToBe(2));
	            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	            log("Switching to new tab",LogType.STEP);
	            getCommand().driver.switchTo().window(listofTabs.get(1));
	            log("Getting new tab page title",LogType.STEP);
	    		String CurrentpageTitle = getCommand().driver.getTitle();
	    	
	    		if(!pageTitle.equals(CurrentpageTitle))
	    		{	    			
	    			log("Clicking on breadcrumb "+BreadCrumbText+" redirecting correctly",LogType.STEP);
	    		}
	    		else
	    		{    			
	    			log("Clicking on breadcrumbs links not redirecting correctly",LogType.ERROR_MESSAGE);
	    			Assert.fail("Clicking on breadcrumbs links not redirecting correctly");
	    		}
	    		
	    		getCommand().driver.close();
	    		getCommand().driver.switchTo().window(listofTabs.get(0));
			}
			
			getCommand().isTargetPresent(StoreLocator_PDPPage);
			getCommand().executeJavaScript("arguments[0].click();", StoreLocator_PDPPage);   
			//getCommand().click(StoreLocator_PDPPage);
			
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id='store-locator']/div/h1")));
			//getCommand().waitForTargetVisible(StoreLocatorHeader, 120);
			
			String StorelocatorpageTitle = getCommand().driver.getTitle();
			String PageText = getCommand().getText(StoreLocatorHeader);
			
			if(!pageTitle.equals(StorelocatorpageTitle) && StorelocatorpageTitle.contains(PageText))
			{	    			
				log("Clicking on Find a store redirecting correctly",LogType.STEP);
			}
			else
			{    			
				log("Clicking on Find a store not redirecting correctly",LogType.ERROR_MESSAGE);
				Assert.fail("Clicking on Find a store not redirecting correctly");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	//Method to verify CompareOverlay Display and Disappear
	public Kohler_ProductDisplayPage VerifyCompareOverlayDisplayandDisappear(String Data) throws InterruptedException
	{
		getCommand().driver.manage().deleteAllCookies();
		try
		{
			SearchData search = SearchData.fetch(Data);
		    String ProductID = search.ProductSku;	    
		    log("Searching for product: "+ ProductID,LogType.STEP);
		    
		    if(getCommand().isTargetVisible(CompareOverlay_Expand))
		    {
		    	getCommand().executeJavaScript("arguments[0].click();", CompareOverlay_Expand)	;    	
		    	getCommand().isTargetPresent(CompareOverlay_ClearAll);				
				getCommand().click(CompareOverlay_ClearAll);
		    }
		    
			Search(ProductID);
					
			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}
			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			
            String browserName = caps.getBrowserName();
			
			if(browserName.equals("MicrosoftEdge"))
			{
				getCommand().waitForTargetVisible(Product, 120);
				getCommand().isTargetPresent(Product);
				getCommand().click(Product);
			}
			
			getCommand().waitForTargetVisible(CompareButton, 100);
			log("Clicking on Compare button in PDP page",LogType.STEP);
			getCommand().isTargetPresent(CompareButton);
			getCommand().executeJavaScript("arguments[0].click();", CompareButton)	;  
			//getCommand().click(CompareButton);
			getCommand().waitForTargetVisible(CompareOverlay, 120);
	        log("Checking Compare Overlay is displayed after clicking on +Compare button",LogType.STEP);
			if(getCommand().isTargetVisible(CompareOverlay))
			{
				log("Compare Overlay is displayed",LogType.STEP);
				
				String Text = getCommand().getText(CompareOverlayProductdetails);
				
				Text = Text+"-0";
				
				Assert.assertEquals(Text, ProductID, "Mismatch in Product that was added for compare. Actual; "+Text+" Expected: "+ProductID);
				
				log("Product is added to compare",LogType.STEP);
			}
			
			else
			{
				log("Compare Overlay is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Compare Overlay is not displayed");
			}
			
			log("Clicking on Clear all option from Compare overlay display",LogType.STEP);
			getCommand().isTargetPresent(CompareOverlay_ClearAll);
			
			getCommand().click(CompareOverlay_ClearAll);
			
			log("Checking Compare overlay is dissapeared after Clicking on Clear all option",LogType.STEP);
			if(!getCommand().isTargetVisible(CompareOverlay))
			{
				log("Compare Overlay is dissapear after clicking on clearall",LogType.STEP);
			}
			
			else
			{
				log("Compare Overlay is not dissapear after clicking on clear all",LogType.ERROR_MESSAGE);
				Assert.fail("Compare Overlay is not dissapear after clicking on clear all");
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	//Method to verify AddtoCart_PDPPage
	public Kohler_ProductDisplayPage VerifyAddtoCart_PDPPage(String Data) throws InterruptedException
	{
		try
		{
			String browserName = caps.getBrowserName();
			
			String pageTitle = getCommand().getPageTitle();
            
            if(browserName.equals("MicrosoftEdge"))
			{
            	log("In "+browserName+" browser Checking any product is added to cart already",LogType.STEP);
            	
                getCommand().isTargetPresent(PDP_CartIcon);
				
				getCommand().click(PDP_CartIcon);
				
				getCommand().waitForTargetVisible(CartPage_Header, 120);
				
				if(!getCommand().isTargetVisible(Removeproduct))
				{
					
				}
				
				else
				{
					log("Some Product is already added to cart",LogType.STEP);
					
					log("Deleting the Product from the cart",LogType.STEP);
					
					getCommand().waitForTargetVisible(Removeproduct, 120);
					
					getCommand().isTargetPresent(Removeproduct);
					
					getCommand().click(Removeproduct);			
				}
			}
            
            else
            {           	
            	String CartText = getCommand().getText(PDP_CartIconFlag);
            	
            	if(!CartText.equals("0")) 
    			{				
    				getCommand().isTargetPresent(PDP_CartIconFlag);
    				
    				getCommand().click(PDP_CartIconFlag);
    				
    				getCommand().waitForTargetVisible(Removeproduct, 120);
    				
    				getCommand().isTargetPresent(Removeproduct);
    				
    				getCommand().click(Removeproduct);			
    			}
            }

	        SearchData search = SearchData.fetch(Data);
		    String ProductID = search.ProductSku;
		    log("Searching for product: "+ ProductID,LogType.STEP);
			Search(ProductID);

			if(browserName.equals("MicrosoftEdge"))
			{
				getCommand().waitForTargetVisible(Product, 120);
				getCommand().isTargetPresent(Product);
				getCommand().click(Product);
			}

			getCommand().waitForTargetVisible(PDP_AddToCart, 100);
			
			String CartFlagText = getCommand().getText(PDP_CartIconFlag);
			
			log("Checking Add To Cart button is displayed in PDP page",LogType.STEP);
			if(getCommand().isTargetVisible(PDP_AddToCart)) 
			{
				log("Add To Cart button is displayed in PDP page",LogType.STEP);
				
				log("Clicking on Add To Cart button in PDP page",LogType.STEP);
				
				getCommand().click(PDP_AddToCart);
				
				getCommand().waitForTargetVisible(PDP_AddToCartModalHeader, 120);
				
				String Text =  getCommand().getText(PDP_AddToCartModalHeader);
				
				log("Checking Add To Cart modal is displayed",LogType.STEP);
				
				if(browserName.equals("MicrosoftEdge"))
				{
					if(getCommand().isTargetVisible(PDP_AddToCartModal) && Text.equals("Added to cart"))
					{
						log("Add To Cart modal popup is displayed and product added to cart after clicking on Add to cart button",LogType.STEP);
						log("Clicking on continue shopping inadd to cart modal",LogType.STEP);
						getCommand().isTargetPresent(PDP_AddToCartContinueShopping);						
						getCommand().click(PDP_AddToCartContinueShopping);
					}
					
					else
					{
						log("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button",LogType.ERROR_MESSAGE);
						Assert.fail("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button");
					}
				}
				else
				{
					if(getCommand().isTargetVisible(PDP_AddToCartModal) && Text.equals("ADDED TO CART"))
					{
						log("Add To Cart modal popup is displayed and product added to cart after clicking on Add to cart button",LogType.STEP);
						log("Clicking on continue shopping inadd to cart modal",LogType.STEP);
						getCommand().isTargetPresent(PDP_AddToCartContinueShopping);						
						getCommand().click(PDP_AddToCartContinueShopping);
					}
					
					else
					{
						log("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button",LogType.ERROR_MESSAGE);
						Assert.fail("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button");
					}
				}

				String CartFlagtextafterAddingproduct = getCommand().getText(PDP_CartIconFlag);
				
				log("Checking Quantity of 1 display in the cart icon flag on to top right corner of the page after adding product to cart",LogType.STEP);
				if(getCommand().getText(PDP_CartIconFlag).equals("1") && !CartFlagtextafterAddingproduct.equals(CartFlagText))
				{
					log("Quantity of 1 display in the cart icon flag on to top right corner of the page after adding product to cart",LogType.STEP);
				}
				
				else
				{
					log("Quantity of 1 is not display in the cart icon flag on to top right corner of the page after adding product to cart",LogType.ERROR_MESSAGE);
					Assert.fail("Quantity of 1 is not display in the cart icon flag on to top right corner of the page after adding product to cart");
				}
				
				
				log("Clicking on Cart icon and checking product 5588-0 displays on the page",LogType.STEP);
				
				getCommand().isTargetPresent(PDP_CartIcon);
				
				getCommand().click(PDP_CartIcon);
				
				getCommand().waitForTargetVisible(CartPage_Header, 120);
				
				String Cartpageheadder = getCommand().getText(CartPage_Header);
				
				if(Cartpageheadder.equals("Your Shopping Cart")) 
				{
					log("Cart page displays after clicking on cart icon",LogType.STEP);
					
					String ProductDetails = getCommand().getText(Productdetail_CartPage);
					
					Assert.assertEquals(ProductDetails, ProductID, "Mismatch in product. Product Displayed: "+ ProductDetails + " and expected: "+ProductID);
					
					log(ProductID + " is dispalyed in cart page",LogType.STEP);
					
					log("Clicking on continue shopping in cart page",LogType.STEP);
					
					getCommand().isTargetPresent(ContinueShopping_CartPage);
					getCommand().executeJavaScript("arguments[0].click();", ContinueShopping_CartPage); 
					
					//getCommand().click(ContinueShopping_CartPage);
					
					String pagetitle = getCommand().getPageTitle();
					
					Assert.assertEquals(pagetitle, pageTitle, "Clciking on continue shopping in cart page not navigating to home page");
				}
				else
				{
					log("Cart page is not displayed after clicking on cart icon",LogType.STEP);
					Assert.fail("Cart page is not displayed after clicking on cart icon");
				}		
			}
			
			else
			{
				log("Add To Cart button is not displayed in PDP page",LogType.ERROR_MESSAGE);
				Assert.fail("Add To Cart button is not displayed in PDP page");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	//Method to verify PDPPage Options
	public Kohler_ProductDisplayPage VerifyOptions_PDPPage(String Data) throws InterruptedException
	{	
        try
        {
        	SearchData search = SearchData.fetch(Data);
    	    String ProductID = search.ProductSku;
    	    log("Searching for product: "+ ProductID,LogType.STEP);
    		Search(ProductID);
    		
            String browserName = caps.getBrowserName();
			
			if(browserName.equals("MicrosoftEdge"))
			{
				getCommand().waitForTargetVisible(Product, 120);
				getCommand().isTargetPresent(Product);
				getCommand().click(Product);
			}
    		
    		getCommand().waitForTargetVisible(PairsWell, 100);
    		
    		String Text = getCommand().getText(PairsWell);
    		
    		if(getCommand().isTargetVisible(PairsWell) && Text.equals("PAIRS WELL WITH"))
    		{
    			log("pairs well with options displays", LogType.STEP);
    			
    			List<WebElement> Options = getCommand().driver.findElements(By.xpath("//div[@id='product-detail__pairs-well-with']//following-sibling::a"));
    			
    			log("pairs well is displays with "+Options.size()+" Options", LogType.STEP);
    		}
    		
    		else
    		{
    			log("pairs well with options is not displays", LogType.ERROR_MESSAGE);
    			Assert.fail("pairs well with options is not displays");
    		}
    				
    		log("Checking Required items displayed for product: "+ProductID, LogType.STEP);
    		
    		if(getCommand().isTargetVisible(AddRequiredItems))
    		{
    			log("Required items displays for product: "+ProductID, LogType.STEP);
    			
    			log("Clicking on Add required items button", LogType.STEP);
    			
    			getCommand().waitForTargetVisible(AddRequiredItems,120);
    			
    			getCommand().executeJavaScript("arguments[0].click();", AddRequiredItems);   
    			//getCommand().click(AddRequiredItems);
    			
    			List<WebElement> IncludeItemsList = getCommand().driver.findElements(By.xpath("//*[@id='required-items']/div[@class='additional-item row marg-b-30-sm pad-b-30-sm row--bottom-border'][1]"));
    			
    			getCommand().waitForTargetVisible(RequiredItemsHeader, 120);
    			
    			String Header = getCommand().getText(RequiredItemsHeader);
    			
    			if(Header.equals("REQUIRED ITEMS"))
    			{
    				log("Required items modal displayed with "+IncludeItemsList.size()+" items", LogType.STEP);
    			}
    			
    			else
    			{
    				log("Required items modal not displays with items", LogType.ERROR_MESSAGE);
    				Assert.fail("Required items modal not displays with items");
    			}
    			
    		}
    		
    		else
    		{
    			log("Required items not displays for product: "+ProductID, LogType.ERROR_MESSAGE);
    			Assert.fail("Required items not displays for product: "+ProductID);
    		}
        }
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	//Method to verify Compare product Modal
	public Kohler_ProductDisplayPage VerifyCompareproductModal() throws InterruptedException
	{
		getCommand().driver.manage().deleteAllCookies();
		String browserName = caps.getBrowserName();
		try
		{
			log("Clicking on Kitchens and navigating to sinks section",LogType.STEP);
			
			getCommand().waitForTargetVisible(Kitchens, 120);
			
			getCommand().isTargetPresent(Kitchens);
			
			getCommand().click(Kitchens);
			
			getCommand().waitForTargetVisible(Accessories, 10);
			
			getCommand().isTargetPresent(Accessories);
			
			//getCommand().click(Accessories);
			getCommand().executeJavaScript("arguments[0].click();", Accessories);   
			getCommand().waitFor(3);
			
			// Validated if Email signUP appears
			String Accessoriesvalue=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				Accessoriesvalue = getCommand().getText(SignUpForEmail);			
		 	}
			if(Accessoriesvalue == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			log("Clicking on cast iron sink material",LogType.STEP);
			
			getCommand().waitForTargetVisible(ProductMaterial, 120);
			
			getCommand().isTargetPresent(ProductMaterial);
			
			//getCommand().click(ProductMaterial);
			getCommand().executeJavaScript("arguments[0].click();", ProductMaterial);   
			getCommand().waitFor(3);
			
			// Validated if Email signUP appears
			String ProductMaterialvalue = null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				ProductMaterialvalue = getCommand().getText(SignUpForEmail);			
			}
			if(ProductMaterialvalue == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
				
			
				
				 			
			log("Adding products for compare",LogType.STEP);
			
	        int productscount = 1;
	        
	        getCommand().waitForTargetVisible(ProductGrid, 120);
			
			List<WebElement> Products = getCommand().driver.findElements(By.xpath(".//*[@id='product-category__product-panels']/div"));
			
			if(browserName.equals("MicrosoftEdge") || browserName.equals("firefox") )
			{					
				js.executeScript("arguments[0].scrollIntoView(true);", Products.get(0));
			}
			
			getCommand().waitFor(3);
			
			for(WebElement Product : Products)
			{
				if(productscount<=3)
				{					
					Actions Action = new Actions(getCommand().driver);
					
					Action.moveToElement(Product).build().perform();
					
					getCommand().waitFor(1);
					
					String Xpath = ".//*[@id='product-category__product-panels']/div["+productscount+"]/div/a[2]";
					
					WebElement Compare = getCommand().driver.findElement(By.xpath(Xpath));
					
					if(Compare.isDisplayed())
					{
						Compare.click();
						getCommand().waitFor(1);
//						if(browserName.equals("MicrosoftEdge"))
//						{	
//							getCommand().waitFor(3);
//							getCommand().driver.findElement(By.cssSelector(".icon--chevron-up")).click();
//							getCommand().waitFor(2);
//						}
					}
					productscount++;
				}			
			}
			
			List<WebElement> CompareProductsCount = getCommand().driver.findElements(By.xpath(".//*[@id='compare-products-bar__dropdown-container']/div"));
			
			String Compare = getCommand().getText(CompareOverlay);
			
			if(browserName.equals("MicrosoftEdge"))
			{
				Compare = Compare.substring(0,16);
				if(CompareProductsCount.size()==3 && Compare.equals("Compare Products"))
				{
					Compare(browserName);
				}
				
				else
				{					
					log("Compare panel is not displayed with three products", LogType.ERROR_MESSAGE);
					Assert.fail("Compare panel is not displayed with three products");
				}
			}
			
			else
			{
				if(CompareProductsCount.size()==3 && Compare.equals("COMPARE PRODUCTS"))
				{
					Compare(browserName);
				}
				
				else
				{					
					log("Compare panel is not displayed with three products", LogType.ERROR_MESSAGE);
					Assert.fail("Compare panel is not displayed with three products");
				}
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public void Compare(String Browsername)
	{
		try
		{
			log("Compare panel displayed with three products",LogType.STEP);
			
			log("Clicking om Compare button in Compare overlay display",LogType.STEP);
			
			getCommand().waitForTargetVisible(Compare_btn, 120);
			
			getCommand().isTargetPresent(Compare_btn);
			
			getCommand().click(Compare_btn);
			
			getCommand().waitForTargetVisible(CompareModal_Header,3);
			
			log("Checking Compare modal displayed",LogType.STEP);
			
			String Text = getCommand().getText(CompareModal_Header);
			
			List<WebElement> CompareModalProducts = getCommand().driver.findElements(By.xpath("//*[@id='compareItem']/div[1]/div/div/a"));
			
			if(Browsername.equals("MicrosoftEdge"))
			{
				if(Text.equals("Compare Products") && CompareModalProducts.size()==3)
				{
					log("Compare modal displayed after cliking on compare button",LogType.STEP);
				}
				
				else
				{				
					log("Compare modal is not displayed after cliking on compare button", LogType.ERROR_MESSAGE);
					Assert.fail("Compare modal is not displayed after cliking on compare button");
				}
			}
			
			else
			{
				if(Text.equals("COMPARE PRODUCTS") && CompareModalProducts.size()==3)
				{
					log("Compare modal displayed after cliking on compare button",LogType.STEP);
				}
				
				else
				{				
					log("Compare modal is not displayed after cliking on compare button", LogType.ERROR_MESSAGE);
					Assert.fail("Compare modal is not displayed after cliking on compare button");
				}
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}		
	}
	
	//Method to verify Discontinued Product
	public Kohler_ProductDisplayPage DiscontinuedProduct(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			String ProductID = search.ProductSku;
			String ProductID2 = search.ProductSku1;
			log("Searching for product: "+ ProductID,LogType.STEP);
		    Search(ProductID);
		    
            String browserName = caps.getBrowserName();
			
			if(browserName.equals("MicrosoftEdge"))
			{
				getCommand().waitForTargetVisible(DisContinuedProductSearch, 120);
			    
			    getCommand().refreshPage();
			}
				
			getCommand().waitForTargetVisible(DiscontinuedCrousel, 100);
			
			log("Checking Discontinued product detail page displays for product"+ ProductID,LogType.STEP);
			
			String text = getCommand().getText(DiscontinuedCrousel);
			
			if(getCommand().isTargetVisible(DiscontinuedCrousel) && text.equals("Discontinued"))
			{
				log("Discontinued product detail page displays for product"+ ProductID,LogType.STEP);
			}
			
			else
			{
				log("Discontinued product detail page is not displays for product"+ ProductID, LogType.ERROR_MESSAGE);
				Assert.fail("Discontinued product detail page is not displays for product"+ ProductID);
			}
			
			
			log("Checking Discontinued product Links to replacement parts (if applicable)"+ ProductID,LogType.STEP);
			
			log("Searching for product: "+ ProductID2,LogType.STEP);
			
		    Search(ProductID2);
				
		    getCommand().waitForTargetVisible(Discontinued, 100);
			
			List<WebElement> Links = getCommand().driver.findElements(By.xpath("//*[@id='product-detail__features']/div[2]/p/a"));
			
			if(Links.size()>0)
			{
				log("Links to replacement parts are present",LogType.STEP);
				
				String pageTitle = getCommand().getPageUrl();
				
				for(WebElement Link : Links)
				{
					String LinkText = Link.getText();
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
					Link.sendKeys(selectLinkOpeninNewTab);
		            getCommand().waitFor(6);
		            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
		            log("Switching to new tab",LogType.STEP);
		            getCommand().driver.switchTo().window(listofTabs.get(1));
		    		
		            log("Getting new tab page url",LogType.STEP);
		    		String CurrentpageTitle = getCommand().getPageUrl();
		    		
		    		log("Checking "+LinkText+" to replacement parts is navigated to respective page",LogType.STEP);
		    	
		    		if(pageTitle.equals(CurrentpageTitle))
		    		{	    			
		    			log("Link "+LinkText+" to replacement parts is not working",LogType.ERROR_MESSAGE);
		    			Assert.fail("Link "+LinkText+" to replacement parts is not working");
		    		}
		    		else
		    		{    			
		    			log("Link "+LinkText+" to replacement parts is navigated to respective page",LogType.STEP); 
		    		}
		    		
		    		getCommand().driver.close();
		    		getCommand().driver.switchTo().window(listofTabs.get(0));
				}

			}
			
			else
			{
				log("Links to replacement parts are not present",LogType.ERROR_MESSAGE);
				Assert.fail("Links to replacement parts are not present");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	//Method to verify Sellable Service part
	public Kohler_ProductDisplayPage SellableServicepart(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			String ProductID = search.ProductSku;
			log("Searching for product: "+ ProductID,LogType.STEP);
		    Search(ProductID);
		    
            String browserName = caps.getBrowserName();
			
			if(browserName.equals("MicrosoftEdge"))
			{
				//getCommand().waitForTargetVisible(DisContinuedProductSearch, 120);
								
				getCommand().waitForTargetVisible(Sellablapart, 120);
				
				getCommand().click(Sellablapart);
			    
			    getCommand().refreshPage();
			}

			getCommand().waitForTargetVisible(PDP_AddToCart, 100);
			
			log("Checking Add To Cart button is displayed in PDP page",LogType.STEP);
			if(getCommand().isTargetVisible(PDP_AddToCart)) 
			{
				log("Add To Cart button is displayed in PDP page",LogType.STEP);
				
				log("Clicking on Add To Cart button",LogType.STEP);
				
				getCommand().click(PDP_AddToCart);
				
				String Text =  getCommand().getText(PDP_AddToCartModalHeader);
				
				log("Checking Add To Cart modal is displayed",LogType.STEP);
				if(getCommand().isTargetVisible(PDP_AddToCartModal) && Text.equals("ADDED TO CART"))
				{
					log("Add To Cart modal popup is displayed and product added to cart after clicking on Add to cart button",LogType.STEP);
					log("Clicking on continue shopping in add to cart modal",LogType.STEP);
					getCommand().isTargetPresent(PDP_AddToCartContinueShopping);
					
					getCommand().click(PDP_AddToCartContinueShopping);
				}			
				else
				{
					log("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button",LogType.ERROR_MESSAGE);
					Assert.fail("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button");
				}		
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;		
	}
	
	//Method For SignIn
 	public Kohler_ProductDisplayPage signIn(String Data)
	{		
 		try
 		{
 			LoginData loginData =LoginData.fetch(Data);
 			String UserName = loginData.UserName;
 			String password = loginData.Password;
 			log("Clicking on Sign in Button", LogType.STEP);
 			getCommand().executeJavaScript("arguments[0].click();", btn_SigninHomePage);
 			if(getCommand().isTargetVisible(SignInModalPopup)) {
 				log("sign in modal displays", LogType.STEP);
 				
 				getCommand().clear(EmailAddress);
 				log("Passing Email Address in to Email address text field", LogType.STEP);
 				getCommand().sendKeys(EmailAddress, UserName);
 				
 				getCommand().clear(Password);
 				log("Providing password in to password text field", LogType.STEP);
 				getCommand().sendKeys(Password, password);
 				log("Clicking on Sign in button", LogType.STEP);
 				getCommand().waitFor(3);
 				getCommand().click(btn_Signin);
 				getCommand().waitFor(5);
 			}
 			else {
 				log("sign in modal not displays", LogType.ERROR_MESSAGE);
 				Assert.fail("sign in modal not displays");
 			}
 		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

 	//Method for Search
	public Kohler_ProductDisplayPage Search(String ProductID)
	{
		try
		{
			getCommand().waitForTargetVisible(Search_HomePage, 180);
			getCommand().isTargetPresent(Search_HomePage);
			log("providing the required product sku details in search box",LogType.STEP);
			getCommand().click(Search_HomePage);
			
			getCommand().sendKeys(Search_HomePage, ProductID);
			
			getCommand().isTargetPresent(Searchbtn_HomePage);
			log("Clicking on search button",LogType.STEP);
			getCommand().click(Searchbtn_HomePage);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_ProductDisplayPage JavascripExecutor(WebElement Element, String IsScrollIntoView, String Click)
	{
		try
		{
			if(IsScrollIntoView.equals("Y"))
			{
				JavascriptExecutor js = (JavascriptExecutor) getCommand().getDriver(); 
				js.executeScript("arguments[0].scrollIntoView(true);", Element);
			}
			
			if(Click.equals("Y"))
			{
				JavascriptExecutor js = (JavascriptExecutor) getCommand().getDriver(); 
				js.executeScript("arguments[0].click();", Element);
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	
}
