/**
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 *																																		   	*
 * 2011-2012 Infosys Limited, Banglore, India. All Rights Reserved																			*
 * Version: 2.0																																*
 * 																																			*
 * Except for any free or open source software components embedded in this Infosys proprietary software program ("Program"),				*
 * this Program is protected by copyright laws, international treaties and other pending or existing intellectual property rights in India, *
 * the United States and other countries. Except as expressly permitted, any unautorized reproduction, storage, transmission 				*
 * in any form or by any means (including without limitation electronic, mechanical, printing, photocopying, recording or otherwise), 		*
 * or any distribution of this Program, or any portion of it, may result in severe civil and criminal penalties, 							*
 * and will be prosecuted to the maximum extent possible under the law 																		*
 *																																			*
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 **/
package com.components.pages;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import net.bytebuddy.asm.Advice.Argument;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.Color; 
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.session.CapabilitiesFilter;
import org.openqa.selenium.remote.session.CapabilityTransform;

import com.components.repository.SiteRepository;
import com.components.yaml.LoginData;
import com.components.yaml.SearchData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;


public class Kohler_HomePage extends SitePage
{

	/* Defining the locators on the Page */ 

	public static final Target GalleryPageTitle=new Target("GalleryPageTitle","//div[@class='curalate-header-copy']",Target.XPATH);
	public static final Target Link_HelpUsToImproveMore = new Target("link_HelpUsToImpMore","//a[@id='kampylink']",Target.XPATH);	
	public static final Target text_commenttextbox = new Target("text_commenttextbox","//textarea[@id='textarea']",Target.XPATH);	
	public static final Target btn_sendmyComments = new Target("btn_comments","//button[@class='submitButton']",Target.XPATH);	
	public static final Target btn_suggestion = new Target("btn_Sugg","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[1]/label/span/span",Target.XPATH);	
	public static final Target btn_dislike = new Target("btn_dlike","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[2]/label/span/span",Target.XPATH);	
	public static final Target btn_praise = new Target("btn_praise","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[3]/label/span/span",Target.XPATH);	
	public static final Target btn_Rating = new Target("btn_rating","//*[@id= 'ItemId-35770-2']",Target.XPATH);	
	public static final Target btn_closeTheWind = new Target("btn_closeTheWind","//*[@id= 'Finish']",Target.XPATH);	
	public static final Target text_homePageTitle = new Target("text_title","//*[@id= 'Finish']",Target.XPATH);	

	public static final Target text_UtilityBarSignin = new Target("text_signin","//*[@id= 'user-name-tray']",Target.XPATH);	
	public static final Target text_UtilityBarMyFld = new Target("text_MyFolder","//a[@title= 'View My Folders']",Target.XPATH);	
	public static final Target text_UtilityBarContactUs = new Target("text_ContactUs","//*[text()= 'Contact Us']",Target.XPATH);	
	public static final Target text_UtilityBarFindAPro = new Target("text_FindAPro","//*[text()= 'Find a Pro']",Target.XPATH);	
	public static final Target text_UtilityBarFindAStore = new Target("text_FindAStore","//*[text()= 'Find a Store']",Target.XPATH);	
	public static final Target element_utilityBar = new Target("ele_Utilitybar","//*[@id= 'sign-in-bar']",Target.XPATH);	
	public static final Target button_slick_prev = new Target("button_slickPre","//*[@id='page-content-home']//following::button[text()='Previous']",Target.XPATH);	
	public static final Target button_slick_next = new Target("button_slickNxt","//*[@id='page-content-home']//following::button[text()='Next']",Target.XPATH);	
	public static final Target Foldertext = new Target("Foldertext","(//a[contains(text(),'TestFolderCreation')])[2]",Target.XPATH);
	public static final Target Foldertexts = new Target("Foldertexts","//a[contains(text(),'TestFolderCreation')]",Target.XPATH);
	public static final Target foldername = new Target("Foldername","(//a[contains(text(),'TestFolderCreation')])[1]",Target.XPATH);

	public static final Target button_slick_next1 = new Target("button_slick_next1","//*[@id='page-content-home']/div[1]/div[1]/div/div/div[5]/div/div/div/div",Target.XPATH);
	public static final Target FindaStore = new Target("FindaStore","//*[@id='store-locator']/div/h1",Target.XPATH);	
	
	/* Defining the locators for SignIn */
	public static final Target btn_nxt=new Target("btn_nxt","//button[@class='buttonNav btnNext one-quarter enabled']",Target.XPATH);
	public static final Target text_email=new Target("text_email","//input[@type='text']",Target.XPATH);
	public static final Target ele1=new Target("text_email","//*[@id='page-content-home']//following::button[1]",Target.XPATH);
	public static final Target ele2=new Target("text_email","//*[@id='page-content-home']//following::button[2]",Target.XPATH);
	public static final Target btn_SigninHomePage  = new Target("btn_SigninHomePage","//*[@id='sign-in-bar__inner']/button[2]",Target.XPATH);
	public static final Target SignInModalPopup  = new Target("SignInModalPopup","//*[@id='modal--sign-in']/div",Target.XPATH);
	public static final Target EmailAddress  = new Target("EmailAddress","//*[@id='traySignInForm']/fieldset/div/input[@id='email']",Target.XPATH);
	public static final Target Password  = new Target("Password","//*[@id='traySignInForm']/fieldset/div/input[@id='password']",Target.XPATH);
	public static final Target btn_Signin  = new Target("btn_Signin","//*[@id='trayProfileSignIn']",Target.XPATH);
	public static final Target UserNameTray  = new Target("UserNameTray","//*[@id='user-name-tray']",Target.XPATH);
	public static final Target UserNameTrayDropDown  = new Target("UserNameTrayDropDown","//*[@id='sign-in-bar__account-dropdown']/div",Target.XPATH);
	public static final Target MyAccount  = new Target("MyAccount","//*[@id='sign-in-bar__account-dropdown']/div/a[1]",Target.XPATH);	
	public static final Target SignOut  = new Target("SignOut","//*[@id='sign-in-bar__account-dropdown']/div/a[2]",Target.XPATH);
	public static final Target FirstName_MyAccount  = new Target("FirstName_MyAccount","//*[@id='tabbed-results__account-details']/div/div[3]/div/div[2]/p",Target.XPATH);
	public static final Target EditAccount  = new Target("EditAccount","//*[@id='account-details--button']",Target.XPATH);
	public static final Target ActualAccountFieldDetail = new Target("ActualAccountFieldDetail","//*[@id='tabbed-results__account-details']/div/div[1]/div/div[7]/p",Target.XPATH);
	public static final Target AccountFieldtoEdit = new Target("AccountFieldtoEdit","//*[@id='describe-yourself-dropdown-page_title']",Target.XPATH);
	public static final Target ReEnterpassword = new Target("ReEnterpassword","//*[@id='account-details--edit']/div[3]/div/div[15]/div[1]/label",Target.XPATH);	
	public static final Target SaveEditedAccount = new Target("SaveEditedAccount","//*[@id='account-details--edit']/div[3]/div/div[1]/div[2]/a[2]/input[1]",Target.XPATH);
	public static final Target SaveSuccess = new Target("SaveSuccess","//*[@id='updateSuccess']/span",Target.XPATH);	
	public static final Target BlueBanner = new Target("BlueBanner","//*[@id='header']/div[2]/div[1]/div",Target.XPATH);

	/* Defining the locators for MyFolder */
	public static final Target MyFolder= new Target("MyFolder","//*[@id='sign-in-bar__inner']/span/a",Target.XPATH);
	public static final Target CreateFolder= new Target("CreateFolder","//*[@id='my-folders']/div[3]/div/div/div/a",Target.XPATH);
	public static final Target FolderName= new Target("FolderName","(//*[@id='folder-name'])",Target.XPATH);
	public static final Target FolderName1= new Target("FolderName1","(//*[@id='folder-name'])[2]",Target.XPATH);
	public static final Target CreateFolderNotes= new Target("CreateFolderNotes","//*[@id='textarea-140']",Target.XPATH);
	public static final Target btn_CreateFolder= new Target("btn_CreateFolder","//*[@id='createNewFolder']//button[1]",Target.XPATH);
	public static final Target ActualFolderName= new Target("btn_CreateFolder","//*[@id='shareFolderName']",Target.XPATH);
	public static final Target MyFolderItemsGrid = new Target("BlueBanner","//*[@id='my-folders-detail__items']/div[1]/div/div/div[2]/div[2]/p[@class='order-detail-table__sku']",Target.XPATH);

	public static final Target MyFolderImage = new Target("MyFolderImage","//*[@id='folderMainImage']",Target.XPATH);
	public static final Target MyFolderCost = new Target("MyFolderCost","//*[@id='my-folders']/div[1]/div/div[2]/div/div[2]/p[1]",Target.XPATH);
	public static final Target MyFolderNotes = new Target("MyFolderNotes","//*[@id='editFolderNote']",Target.XPATH);
	public static final Target MyFolderItemsListing = new Target("MyFolderItemsListing","//*[@id='my-folders']/div[3]/div/div/div/div[3]",Target.XPATH);
	public static final Target CheckBox = new Target("CheckBox","//*[@id='my-folders-detail__items']/div[1]/div[1]/div/div[1]/label/span[1]",Target.XPATH);
	public static final Target CheckBox1 = new Target("CheckBox1","//*[@id='my-folders']/div[3]/div/div/div/div[2]/div[2]/label/span[1]",Target.XPATH);

	public static final Target SecondCheckBox = new Target("SecondCheckBox","//*[@id='my-folders-detail__items']/div[1]/div[2]/div/div[1]/label/span[1]",Target.XPATH);
	public static final Target ItemText = new Target("ItemText","//*[@id='my-folders-detail__items']/div[1]/div[1]/div/div[2]/div[2]/p[1]",Target.XPATH);
	public static final Target SecondItemText = new Target("ItemText","//*[@id='my-folders-detail__items']/div[1]/div[2]/div/div[2]/div[2]/p[1]",Target.XPATH);

	public static final Target CopyMyFolder_Signedin = new Target("CopyMyFolder_Signedin","//*[@id='my-folders']/div[3]/div/div/div/div[2]/div[4]/div/button[1]",Target.XPATH);
	public static final Target MoveMyFolder_Signedin = new Target("MoveMyFolder_Signedin","//*[@id='my-folders']/div[3]/div/div/div/div[2]/div[4]/div/button[2]",Target.XPATH);
	public static final Target Add_CopyItem= new Target("Add_CopyItem","//*[@id='copyToFolder']/button[1]",Target.XPATH);

	public static final Target ShareFolderOption= new Target("ShareFolderOption","//*[@id='my-folders']/div[1]/div/div[2]/div/div[4]/div/button[4]",Target.XPATH);
	public static final Target Share_EmailAFriend= new Target("Share_EmailAFriend","//*[@id='modal-myfolder-email-friend']/div/h4",Target.XPATH);
	public static final Target Share_EmailAFriendClose= new Target("Share_EmailAFriendClose","(//div[@class='modal__inner']/button/i)[10]",Target.XPATH);
	public static final Target Share_ShareKohlerClose= new Target("Share_ShareKohlerClose","//*[@id='modal-with-showroom']/div/button/i",Target.XPATH);
	public static final Target Share_KohlerShowroom= new Target("Share_KohlerShowroom","//*[@id='modal-with-showroom']/div/h4",Target.XPATH);
	public static final Target CopyFolderOption= new Target("CopyFolderOption","//*[@id='my-folders']/div[1]/div/div[2]/div/div[4]/div/button[2]",Target.XPATH);	
	public static final Target SaveAsNewFolder= new Target("SaveAsNewFolder","//*[@id='saveAsNewFolder']/button[2]",Target.XPATH);
	public static final Target DeleteFolderOption= new Target("DeleteFolderOption","//*[@id='my-folders']/div[1]/div/div[2]/div/div[4]/div/button[1]",Target.XPATH);
	public static final Target DeleteFolderConfirmation= new Target("DeleteFolderConfirmation","//*[@id='removeFolder']/button[1]",Target.XPATH);

	/* Defining the locators for Kohler Ideas grid */ 
	public static final Target KohlerIdeaslayout_LeftArrow  = new Target("KohlerIdeaslayout_LeftArrow","//*[@id='curalate-content']/button[1]",Target.XPATH);
	public static final Target KohlerIdeaslayout_RightArrow = new Target("KohlerIdeaslayout_RightArrow","//*[@id='curalate-content']/button[2]",Target.XPATH);
	public static final Target KohlerIdeaslayout_ViewGallery = new Target("KohlerIdeaslayout_ViewGallery","curalate-view-gallery",Target.CLASS);
	public static final Target KohlerIdeaslayout_SubmitaPhoto = new Target("KohlerIdeaslayout_SubmitaPhoto","//div[@class='curalate-footer']/button",Target.XPATH);
	public static final Target KohlerIdeaslayout_Uploader = new Target("KohlerIdeaslayout_Uploader","//div[@class='desktop-content content-area active']",Target.XPATH);
	public static final Target KohlerIdeaslayout = new Target("KohlerIdeaslayout","curalate-thumbs",Target.CLASS);
	public static final Target KohlerImage = new Target("KohlerImage","//div[3][contains(@id,'photo-')]/div/img",Target.XPATH);

	/* Defining the locators for Overlay popup on the Page */ 
	public static final Target OverlayDisplayCross = new Target("OverlayDisplayCross","(//button[contains(@class,'curalate-modal-close')])[2]",Target.XPATH);
	public static final Target OverlayDisplay_RightArrow = new Target("OverlayDisplay_RightArrow","/html/body/div[17]/div/div[1]/button[2]",Target.XPATH);
	public static final Target OverlayDisplay_LeftArrow = new Target("OverlayDisplay_LeftArrow","/html/body/div[17]/div/div[1]/button[1]",Target.XPATH);
	public static final Target OverlayDisplay_UserName= new Target("OverlayDisplay_UserName","curalate-username",Target.CLASS);
	public static final Target OverlayDisplay_Share= new Target("OverlayDisplay_Share","curalate-share-header",Target.CLASS);	
	public static final Target OverlayDisplay_Share_F= new Target("OverlayDisplay_Share_F","/html/body/div[17]/div/div[3]/div[2]/div[2]/button[1]",Target.XPATH);
	public static final Target OverlayDisplay_Share_T= new Target("OverlayDisplay_Share_T","/html/body/div[17]/div/div[3]/div[2]/div[2]/button[2]",Target.XPATH);
	public static final Target OverlayDisplay_Share_P= new Target("OverlayDisplay_Share_P","/html/body/div[17]/div/div[3]/div[2]/div[2]/button[3]",Target.XPATH);
	public static final Target OverlayDisplay_ShopHeader= new Target("OverlayDisplay_ShopHeader","curalate-shop-header",Target.CLASS);
	public static final Target OverlayDisplay_SmallProductImage= new Target("OverlayDisplay_SmallProductImage","curalate-product-image",Target.CLASS);
	public static final Target OverlayDisplay_SmallProductDescription= new Target("OverlayDisplay_SmallProductDescription","curalate-product-name",Target.CLASS);
	public static final Target OverlayDisplay_SmallProductNxt= new Target("OverlayDisplay_SmallProductNxt","curalate-products-carousel-next",Target.CLASS);
	public static final Target OverlayDisplay_SmallProductPrev= new Target("OverlayDisplay_SmallProductPrev","curalate-products-carousel-prev",Target.CLASS);

	/* Defining the locators for Promo grid on the Page */ 
	public static final Target PromoModuleGrid= new Target("PromoModuleGrid","//*[@class='container' ]/ul/li[@class='promotion']/a",Target.XPATH);
	public static final Target BackToKohler= new Target("BackToKohler","backToLink",Target.ID);

	/* Defining the locators for Discover the Possibilities grid on the Page */
	public static final Target NextArrow_DiscoverthePossibilities= new Target("NextArrow_DiscoverthePossibilities","//*[@id='container-home']/div[3]/div/div/div[1]/button[2]",Target.XPATH);
	public static final Target PrevArrow_DiscoverthePossibilities= new Target("PrevArrow_DiscoverthePossibilities","//*[@id='container-home']/div[3]/div/div/div[1]/button[1]",Target.XPATH);	
	public static final Target AddToFolderTextArea= new Target("AddToFolderTextArea","//*[@id='addToFolder']/div[4]/div/textarea",Target.XPATH);	
	public static final Target AddToFolderTextArea_Signin= new Target("AddToFolderTextArea_Signin","//*[@id='addToFolder']/div[7]/div/textarea",Target.XPATH);
	public static final Target AddToFolderDropDown= new Target("AddToFolderDropDown","//*[@class='modal__inner modal-size-816']/form/div[3]/div/div[2]/div/span[2]",Target.XPATH);
	public static final Target AddToFolderDropDown1= new Target("AddToFolderDropDown1","//*[@id='copyToFolder']/button[1]",Target.XPATH);
	public static final Target AddToFolderSubmitButton= new Target("AddToFolderSubmitButton","//*[@id='addToFolder']/button[1]",Target.XPATH);
	public static final Target AddToFolderContinueShopping= new Target("AddToFolderContinueShopping","//*[@id='showThankyou']/div[2]/div/button[2]",Target.XPATH);	
	public static final Target MyKohlerFolder= new Target("MyKohlerFolder","//*[@id='my-folders']/div[3]/div/div[2]/div/a/img",Target.XPATH);
	public static final Target AddToFolder= new Target("AddToFolder","//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div[2]/div[6]/div[1]/button[1]",Target.XPATH);
	public static final Target EditTextArea_Folder= new Target("EditTextArea_Folder","//*[@id='textarea-140-3']",Target.XPATH);
	public static final Target EditSave_Folder= new Target("EditSave_Folder","//*[@id='editItemNote']/button[1]",Target.XPATH);
	public static final Target Deletebtn_Folder= new Target("Deletebtn_Folder","//*[@id='my-folders']/div[3]/div/div/div/div[2]/div[4]/div/button[3]",Target.XPATH);
	public static final Target Deletebtn_Folder1= new Target("Deletebtn_Folder1","//*[@id='my-folders']/div[3]/div/div/div/div[1]/div[4]/div/button",Target.XPATH);
	public static final Target SelectAllCheckBox= new Target("SelectAllCheckBox","//*[@id='my-folders']/div[3]/div/div/div/div[2]/div[2]/label/span[1]",Target.XPATH);
	public static final Target SelectAllCheckBox1= new Target("SelectAllCheckBox1","//*[@id='my-folders']/div[3]/div/div/div/div[1]/div[2]/label/span[1]",Target.XPATH);
	public static final Target MyFolderHeader = new Target("MyFolderHeader","//*[@id='my-folders']/div[1]/h1",Target.XPATH);
	public static final Target DeleteConfirmation= new Target("DeleteConfirmation","//*[@id='deleteItems']",Target.XPATH);
	public static final Target TestFolderCreation1= new Target("TestFolderCreation1","(//a[contains(text(),'TestFolderCreation1')])",Target.XPATH);
	public static final Target SignUpForEmail = new Target("SignUpForEmail","(//*[@id='signupModal']//following::div[@class='main-content-email-pop']/span[1])",Target.XPATH);
	public static final Target SignUpForEmailClose = new Target("SignUpForEmailClose","(//*[@id='signupModal']//following-sibling::div[@class='modal-content']/span[1])",Target.XPATH);
	public static final Target KohlerLogo = new Target("KohlerLogo",("//img[@id='main-nav__logo']"),Target.XPATH);
	
	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities(); 
	JavascriptExecutor js =  (JavascriptExecutor)getCommand().driver;
	Actions Action = new Actions(getCommand().getDriver());

	public Kohler_HomePage(SiteRepository repository)
	{
		super(repository);
	}

	/* Functions on the Page are defined below */

	public Kohler_HomePage atHomePage()
	{
		log("Launched Kohler Site",LogType.STEP);
		return this;

	}
	
	// SignUp pop-up on Home Page
	 	public Kohler_HomePage SignUpPopUp()
	 	{
	 		boolean flag = getCommand().isTargetVisible(KohlerLogo);
	 		if(flag)
	 		{
	 			Assert.assertTrue(flag, "Unable to view the Kohler Logo");
	 		}
	 		else
	 		{
	 			String value = getCommand().getText(SignUpForEmail);			
	 	 		if(value != null)
	 	 			log("Alert is not present on HomePage",LogType.STEP);
	 	 		else
	 	 			log("Alert is present on home page",LogType.STEP);
	 	 			getCommand().click(SignUpForEmailClose);
	 	 			
	 		}
	 		
	 			return this;
	 	}
	 	
	
	// Verify the list of links in Home Page Footer
	public Kohler_HomePage VerifyFooterLinks()
	{
		String pageURL = "";
		getCommand().driver.manage().window().maximize();
		
		log("Verifying HomePage_fotter Links",LogType.STEP);
		JavascriptExecutor js = (JavascriptExecutor)getCommand().driver;
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		
		// get the list of links available in footer
		List<WebElement> footer = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[2]/div/div[@class='col-1-of-5-md col-full-sm']"));
		int  footerlinks_count = footer.size();
		try 
		{
			for (int j = 1; j <= footerlinks_count; j++) 
			{
				// get the list of footer header
									
				List<WebElement> footer_header = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[2]/div/div["+j+"]"));
				int footerHeaderlinks_count = footer_header.size();
				for (int k = 1; k <= footerHeaderlinks_count; k++)
				{
					// get the list of links under each column
					List<WebElement> footer_elements = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[2]/div/div["+j+"]/div[@id ='footer__tab--"+j+"']/a"));
					int footerColLinks_count = footer_elements.size();
					for (int l = 0; l < footerColLinks_count; l++) 
					{
						
						int Win_size = getCommand().driver.getWindowHandles().size();
						log("Open Windows Size is :" + Win_size ,LogType.STEP);
						
						log(footer_elements.get(l).getText(),LogType.STEP);
						String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
						
						log("Verify the color on hover",LogType.STEP);
						Actions builder = new Actions(getCommand().driver);
						builder.moveToElement(footer_elements.get(l)).build().perform();
						
						getCommand().waitFor(3);
						String hoverOnColor  = footer_elements.get(l).getCssValue("color");
						
						
						String browserName = caps.getBrowserName();

						if(browserName.equals("MicrosoftEdge")|| browserName.equalsIgnoreCase("firefox"))
						{
							String[] color1 =hoverOnColor.replace("rgb(", "").replace(")", "").split(",");
							 String hex = String.format("#%02x%02x%02x", Integer.parseInt(color1[0].trim()), Integer.parseInt(color1[1].trim()), Integer.parseInt(color1[2].trim()));
							 log("Convert rgb to hex : " + hex.toUpperCase(),LogType.STEP); 
								
					           if(hex.equals("#FFFFFF"))
	     						Assert.assertEquals(hex, "#FFFFFF", "link is not lighten on hover");
								
						
						}

						else
						{
							String[] color1 =hoverOnColor.replace("rgba(", "").replace(")", "").split(",");
							 String hex = String.format("#%02x%02x%02x", Integer.parseInt(color1[0].trim()), Integer.parseInt(color1[1].trim()), Integer.parseInt(color1[2].trim()));
							 log("Convert rgb to hex : " + hex.toUpperCase(),LogType.STEP); 
								
					           if(hex.equals("#FFFFFF"))
	     						Assert.assertEquals(hex, "#FFFFFF", "link is not lighten on hover");
								
						
						}
						
						
						footer_elements.get(l).sendKeys(selectLinkOpeninNewTab);
						getCommand().waitFor(2);
						ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
						WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
						wait.until(ExpectedConditions.numberOfwindowsToBe(2));							
						
						getCommand().driver.switchTo().window(tabs2.get(1));
						Win_size = getCommand().driver.getWindowHandles().size();
					    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opened in new window");
					    		    
					    pageURL = getCommand().driver.getCurrentUrl();
					    
					    log("Page URL is:" + pageURL,LogType.STEP);
					   
					    	getCommand().driver.close();
					    	getCommand().driver.switchTo().window(tabs2.get(0));
					    	getCommand().waitFor(3);
							
					}
				
				
				}
			}
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		

		return this;
	}

	// Verify HelpUsToImproveMore link functionality
	public Kohler_HomePage VerifyHelpUsToImproveMore(String text, String feedback)
	{
		log("Click on HelpusToImproveMore",LogType.STEP);
		try {
				
				log("MouseHover on HelpUsToImproveMore link",LogType.STEP);
				JavascriptExecutor js = (JavascriptExecutor)getCommand().driver;
				js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
				
				getCommand().click(Link_HelpUsToImproveMore);
				getCommand().waitFor(10);
				
				ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
				getCommand().driver.switchTo().window(tabs2.get(1));
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='submitButton']")));
				//getCommand().waitForTargetVisible(btn_suggestion, 120);
				
			    String pageTitle = getCommand().driver.getTitle();
			    
			    log("Page title is:" + pageTitle,LogType.STEP);
			    Assert.assertEquals(pageTitle, "Kohler US Study", "Title mismatch");
			    
			    log("select the type of feedback" , LogType.STEP);
			    
			    switch (feedback) {
				case "Suggestion": 
					 	getCommand().waitForTargetVisible(btn_suggestion);
					    getCommand().click(btn_suggestion);
					break;
				case "Dislike": 
				 	getCommand().waitForTargetVisible(btn_dislike);
				    getCommand().click(btn_dislike);
				    break;
				case "Praise": 
				 	getCommand().waitForTargetVisible(btn_praise);
				    getCommand().click(btn_praise);
				    break;

				default:
					break;
				}
			    
			    log("Input comments in comments section" , LogType.STEP);
			    getCommand().sendKeys(text_commenttextbox,text);
			    
			    String browserName = caps.getBrowserName();

			    if(browserName.equals("MicrosoftEdge"))
			    {
			    	getCommand().waitForTargetPresent(btn_nxt);
			    	
					  getCommand().click(btn_nxt);
					    
					    log("Select the rating based on feedback" , LogType.STEP);
					    getCommand().waitForTargetVisible(btn_Rating);
					    getCommand().click(btn_Rating);
					    
					    getCommand().waitForTargetPresent(text_email);
					    getCommand().sendKeys(text_email,text);
					  
					    
					    getCommand().waitForTargetPresent(btn_nxt);						    
					    getCommand().click(btn_nxt);
					   
					
			    }

			    else
			    {
			   
			    
			    log("Select the rating based on feedback" , LogType.STEP);
			    getCommand().waitForTargetVisible(btn_Rating);
			    getCommand().click(btn_Rating);
			    
			    
			    
			    log("click on sendmycomments button" , LogType.STEP);
			    getCommand().waitForTargetVisible(btn_sendmyComments);
			    getCommand().click(btn_sendmyComments);
			  
			    
			    }
			    
			    log("click on Close the window button" , LogType.STEP);
			    getCommand().waitForTargetVisible(btn_closeTheWind);
			    getCommand().click(btn_closeTheWind);
			    
			    log("Switch to main Window" , LogType.STEP);
			    getCommand().driver.switchTo().window(tabs2.get(0));
			    String actualTitle = getCommand().driver.getTitle();
			    Assert.assertEquals(actualTitle,"KOHLER | Toilets, Showers, Sinks, Faucets and More for Bathroom & Kitchen", "Home Page Title mismatch");
			    
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	// verify Home Page title
	public Kohler_HomePage verifyHomePageTitle()
	{
		try {
			log("Verify the Home Page Title",LogType.STEP);
			Assert.assertEquals(getCommand().driver.getTitle() ,"KOHLER | Toilets, Showers, Sinks, Faucets and More for Bathroom & Kitchen","Home Page Title mismatch");
			log("Home Page Title is present and verified",LogType.STEP);
	}catch(Exception ex)
	{
		Assert.fail(ex.getMessage());
	}
	
	return this;
	}

	// Verify Home Page URL
	public Kohler_HomePage verifyHomePageURL()
	{
		try {
			boolean condtion= true;
			log("Verify the Home Page URL",LogType.STEP);
			String URL = getCommand().driver.getCurrentUrl();
			if(URL.contains("https://"))
			Assert.assertTrue(condtion);
			log("Home Page URL is verified",LogType.STEP);
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
	
	return this;
}

	// Verify utility Bar color layout and content
	public Kohler_HomePage VerifyUtilitybarLinks()
	{
		try {
			
			log("Verify the Kohler HomePage URL",LogType.STEP);
			String KohlerURL = getCommand().getPageUrl();
			
			log("Verify the utility bar titles",LogType.STEP);
			WebElement bar = getCommand().driver.findElement(By.xpath("//*[@id='sign-in-bar']//following::div[@id='sign-in-bar__inner']"));
		
			List<WebElement> utilityBar_links = bar.findElements(By.tagName("a"));
			List<WebElement> utilityBar_link1 = bar.findElements(By.tagName("button"));
		
			log("Add all the elements in a list and iterate through each link",LogType.STEP);
			List<WebElement> utilityBarLinks = new ArrayList<WebElement>();
			utilityBarLinks.addAll(utilityBar_links);
			utilityBarLinks.addAll(utilityBar_link1);
		
					
			for(WebElement ele :utilityBarLinks )
			{
			
				if(!ele.getText().isEmpty())
				{
				
					String text = ele.getText();
					String href = ele.getAttribute("href");
					log("Verify the text of each link: "+ text,LogType.STEP);
				
					log("Verify the color of each link: "+ ele.getCssValue("color"),LogType.STEP);
					log("Verify the font size of each link: "+ ele.getCssValue("font-size") ,LogType.STEP);
										
					int Win_size = getCommand().driver.getWindowHandles().size();
					log("Verify the size of open windows currently: "+ Win_size,LogType.STEP);
				
					Actions action = new Actions(getCommand().driver);
					action.moveToElement(ele).build().perform();
										
					String buttonTextColor = ele.getCssValue("color");
					String hex = Color.fromString(buttonTextColor).asHex();
					log("Hover on each element and get the color: "+ hex,LogType.STEP);
					if(ele.getText().equals("Sign In"))
					{
						
						ele.click();
						if(getCommand().driver.findElement(By.xpath("//div[@id='modal--sign-in']")).isDisplayed())
						{
							
							getCommand().driver.findElement(By.xpath("//div[@id='modal--sign-in']//following::button[@class='modal__close-button']")).click();
						}
					
					
					}
					else
					{
						log("click on each link and verify in new tab",LogType.STEP);
						String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
						ele.sendKeys(selectLinkOpeninNewTab);
						WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
						wait.until(ExpectedConditions.numberOfwindowsToBe(2));	
						ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
						getCommand().driver.switchTo().window(tabs2.get(1));
						Win_size = getCommand().driver.getWindowHandles().size();
					    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
					    
					    String pageTitle = getCommand().driver.getTitle();
						log("verify the Page title"+ pageTitle,LogType.STEP);
						
						getCommand().driver.close();
						log("Close the tab and switch back to parent window",LogType.STEP);
						getCommand().driver.switchTo().window(tabs2.get(0));
						getCommand().waitFor(3);
					}
				}
			}
		
	}catch(Exception ex)
	{
		Assert.fail(ex.getMessage());
	}
	return this;
}

	// verify worldwide countries expansion and landing on related page
	public Kohler_HomePage VerifyWorldWideCountriesLink()
	{
		try {
			
			log("Verify worldWide Countries link",LogType.STEP);
			WebElement UtilityBar_country = getCommand().driver.findElement(By.xpath("//*[@id='sign-in-bar__link--our-brands-button']"));
			log("Click on Worldwide click in Home Page",LogType.STEP);
			UtilityBar_country.click();
			
			log("reterive through the list of region in worldwide link",LogType.STEP);
			List<WebElement> ListOfAllCountries = getCommand().driver.findElements(By.xpath("//*[@class='container regionlist regionListFlags row']/div"));
			 
			for (int i = 1; i < ListOfAllCountries.size(); i++)
			{
				
				List<WebElement> ListOfCuntries_div = getCommand().driver.findElements(By.xpath("//*[@class='container regionlist regionListFlags row']/div["+i+"]"));
				for (int j = 1; j <= ListOfCuntries_div.size(); j++)
				{
					List<WebElement> div_CountryValues = getCommand().driver.findElements(By.xpath("//*[@class='container regionlist regionListFlags row']/div["+i+"]/ul"));
					
					if(div_CountryValues.size()== 2)
					{						
						for (int k = 1; k <= div_CountryValues.size(); k++)
						{
							List<WebElement> div_Asia = getCommand().driver.findElements(By.xpath("//*[@class='container regionlist regionListFlags row']/div["+i+"]/ul["+k+"]/li"));
							for (int l = 1; l <= div_Asia.size(); l++)
							{
								WebElement div_asiaCountries = getCommand().driver.findElement(By.xpath("//*[@class='container regionlist regionListFlags row']/div["+i+"]/ul["+k+"]/li["+l+"]/a"));
								String countries =div_asiaCountries.getText();
								log("Country value under asia region is :" +countries,LogType.STEP);
																
								int Win_size = getCommand().driver.getWindowHandles().size();
								log("Verify the size of open windows currently: "+ Win_size,LogType.STEP);
								
								log("click on each link and verify in new tab",LogType.STEP);
								//getCommand().waitFor(5);
								String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
								div_asiaCountries.sendKeys(selectLinkOpeninNewTab);
								
								WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
								wait.until(ExpectedConditions.numberOfwindowsToBe(2));	
								
								ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
								
								if(tabs2.size()!=2)
								{
									getCommand().waitFor(3);
									
									String pageTitle = getCommand().driver.getTitle();
									log("verify the Page title"+ pageTitle,LogType.STEP);
			                      
									getCommand().driver.get("https://preprod.us.kohler.com/us/");
									getCommand().waitFor(2);
									
								
									
									WebElement UtilityBar_country1 = getCommand().driver.findElement(By.xpath("//*[@id='sign-in-bar__link--our-brands-button']"));

									UtilityBar_country1.click();
									getCommand().waitFor(2);
									continue;								
								}
	
								getCommand().driver.switchTo().window(tabs2.get(1));
								Win_size = getCommand().driver.getWindowHandles().size();
							    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
							    
								String pageTitle = getCommand().driver.getTitle();
								log("verify the Page title"+ pageTitle,LogType.STEP);
								
								getCommand().driver.close();
								log("Close the tab and switch back to parent window",LogType.STEP);
								getCommand().driver.switchTo().window(tabs2.get(0));
								
								getCommand().waitFor(2);
							}
							
						}
					}else
					{
						List<WebElement> div_restOfAsia = getCommand().driver.findElements(By.xpath("//*[@class='container regionlist regionListFlags row']/div["+i+"]/ul/li"));
						for (int m = 1; m <= div_restOfAsia.size(); m++)
						{
							WebElement restOfAsiaCountries = getCommand().driver.findElement(By.xpath("//*[@class='container regionlist regionListFlags row']/div["+i+"]/ul/li["+m+"]/a"));
							String restOfAsiaCountry =restOfAsiaCountries.getText();
							log("Country apart from asia region is :" +restOfAsiaCountry,LogType.STEP);
							
							int Win_size = getCommand().driver.getWindowHandles().size();
							log("Verify the size of open windows currently: "+ Win_size,LogType.STEP);
							
							log("click on each link and verify in new tab",LogType.STEP);
							String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
							restOfAsiaCountries.sendKeys(selectLinkOpeninNewTab);
							
							
							WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
							wait.until(ExpectedConditions.numberOfwindowsToBe(2));	
							
							ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
							
							if(tabs2.size()!=2)
							{
								getCommand().waitFor(3);
								String pageTitle = getCommand().driver.getTitle();
								log("verify the Page title"+ pageTitle,LogType.STEP);
	                      
								getCommand().driver.get("https://preprod.us.kohler.com/us/");
								getCommand().waitFor(2);

								WebElement UtilityBar_country1 = getCommand().driver.findElement(By.xpath("//*[@id='sign-in-bar__link--our-brands-button']"));

								UtilityBar_country1.click();
								getCommand().waitFor(2);
								continue;							
							}
							
							getCommand().driver.switchTo().window(tabs2.get(1));
							Win_size = getCommand().driver.getWindowHandles().size();
						    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
						    
							String pageTitle = getCommand().driver.getTitle();
							log("verify the Page title"+ pageTitle,LogType.STEP);
							
							getCommand().driver.close();
							log("Close the tab and switch back to parent window",LogType.STEP);
							getCommand().driver.switchTo().window(tabs2.get(0));
						}
					}
					
				}
				
			}

			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	// Verify brand tray disappears when scrolling down
	public Kohler_HomePage VerifyBrandTrayVisibility()
	{
		boolean condition = true;
		try {
				
				log("Verify brand tray visibility on scroll down",LogType.STEP);
				boolean state = getCommand().isTargetVisible(element_utilityBar);
				if(state)
				{
					log("Scroll down if the tray is visible",LogType.STEP);
					JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
					js.executeScript("window.scrollBy(0,3000)");

				}
				
				condition = getCommand().isTargetVisible(element_utilityBar);

				if(!condition)
					log("verify the condition when the brand tray is invisible",LogType.STEP);
					Assert.assertTrue(true, "utitliy bar is not viewable");
					log("verified the condition when the brand tray is invisible",LogType.STEP);

			
			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
		return this;
	}


	//Verify Hero carousel auto rotation and hover
	public Kohler_HomePage VerifyHero()
	{
		try {
			
			String URL = getCommand().getPageUrl();
			List<String> Herocarousel_Slides = new ArrayList<String>();
			String toolTipText=null;
			//int count_slick = 2;
			
		SoftAssert softAssert = new SoftAssert();
			
				log("verify the hover functionality on hero side arrows",LogType.STEP);
			
				WebElement elem1 = getCommand().driver.findElement(By.xpath("//*[@id='page-content-home']//following::button[1]"));
				WebElement elem2 = getCommand().driver.findElement(By.xpath("//*[@id='page-content-home']//following::button[2]"));

				getCommand().waitFor(2);
				
				getCommand().isTargetPresent(ele1);
				getCommand().mouseHover(ele1);
			
				String buttonTextColor = elem1.getCssValue("color");
				
				String hex = Color.fromString(buttonTextColor).asHex();
				log("Hover on slick and get the color: "+ hex,LogType.STEP);
				
				if(hex.equals("#ffffff"))
					log("verify color of elemet on hover on hero side arrows",LogType.STEP);
				
					softAssert.assertEquals(hex,"#ffffff");
					
					
					getCommand().waitFor(2);
					
					
					getCommand().isTargetPresent(ele2);
					getCommand().mouseHover(ele2);
				
									
					String buttonTextColor1 = elem2.getCssValue("color");
					
					String hex1 = Color.fromString(buttonTextColor1).asHex();
					log("Hover on slick and get the color: "+ hex1,LogType.STEP);
					
					if(hex1.equals("#ffffff"))
						log("verify color of elemet on hover on hero side arrows",LogType.STEP);
					
						softAssert.assertEquals(hex1,"#ffffff");
		
			
		
			int count =1;
			log("verify the Hero carousel functionality",LogType.STEP);
			List<WebElement> navDots = getCommand().driver.findElements(By.xpath("//*[@id='page-content-home']/div[1]/div[1]/ul/li/button[1]"));
			int navDot = navDots.size();
			for (int i = 0; i < navDot; i++)
			{
				
					log("verify the list of navigationation buttons on Hero and click on each;",LogType.STEP);
					Actions action = new Actions(getCommand().driver);
					action.click(navDots.get(i)).build().perform();
					
					//getCommand().driver.findElement(By.xpath("//*[@id='page-content-home']/div[1]/div[1]/ul/li["+i+"]/button")).click();
					getCommand().waitFor(2);
					log("Hold the carousel by hovering on it",LogType.STEP);
					getCommand().waitForTargetPresent(button_slick_next1);
					getCommand().mouseHover(button_slick_next1);
					getCommand().waitFor(5);
					String navDotsClass = navDots.get(i).getAttribute("class");
					if(navDotsClass.contains("slick-active"))
					{
						log("verify Tool tip text of each Carousel by hovering on hero side arrows",LogType.STEP);
						toolTipText = getCommand().driver.findElement(By.xpath("//*[@id='page-content-home']/div[1]/div[1]/div/div/div["+count+"]")).getAttribute("title");
						
						
						log("Tool Tip text of carousel is :" +toolTipText ,LogType.STEP);
						count++;
						Herocarousel_Slides.add(toolTipText);
						
				     }
					
				}
				
			
				
				log("verify navigation;",LogType.STEP);
				//List<WebElement> carousels = getCommand().driver.findElements(By.xpath("//*[@id='page-content-home']/div[1]/div[1]/div/div/div")); 
				for (int i =0; i <navDot ; i++)
				{
					
					List<WebElement> ele = getCommand().driver.findElements(By.xpath("//*[@id='page-content-home']/div[1]/div[1]/ul/li/button"));
					Actions action = new Actions(getCommand().driver);
					action.click(ele.get(i)).build().perform();
					
					log("verify navigation to respective pages",LogType.STEP);

					int j=i+1;
							WebElement ele1=getCommand().driver.findElement(By.xpath("//*[@id='page-content-home']/div[1]/div[1]/div/div/div["+j+"]/div/div/div/div/div/h2"));
							js.executeScript("arguments[0].click();", ele1);
							getCommand().waitFor(5);

							  if(!getCommand().driver.getCurrentUrl().equals("https://preprod.us.kohler.com/us/"))
									
						             
							  {
				                  log("on clicking on carousel, application is navigating to respective page :" +getCommand().driver.getCurrentUrl() ,LogType.STEP);
			              }
			       
			                            else
			                            	{
				log("On clicking on carousel,application is not navigating to respective page :" +getCommand().driver.getCurrentUrl() ,LogType.ERROR_MESSAGE);

			}
							  
							    getCommand().driver.get(URL);
	                            getCommand().waitFor(5);
			
				}
	}
					
	
	catch(Exception ex)
	{
		Assert.fail(ex.getMessage());
	}
	return this;
}

	// Verify links to other Kohler brands opens a new window.
	public Kohler_HomePage VerifyLinkOtherKohlerBrands()
	{
		try {
			log("Verify Link Other than Kohler Brands: ",LogType.STEP);
			JavascriptExecutor js = (JavascriptExecutor)getCommand().driver;
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
			
			List<WebElement> Footer_KohlerColinks = getCommand().driver.findElements(By.xpath("//*[@id='footer__tab--2']/a"));
			for(WebElement footer_link: Footer_KohlerColinks)
			{
				if(footer_link.getText().contains("Ann Sacks") ||footer_link.getText().contains("Kallista") || footer_link.getText().contains("Robern") || footer_link.getText().contains("Sterling") || footer_link.getText().contains("Novita"))
				{
					int Win_size = getCommand().driver.getWindowHandles().size();
					log("Verify the size of open windows currently: "+ Win_size,LogType.STEP);
					
					log("click on each link and verify in new tab",LogType.STEP);
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
					footer_link.sendKeys(selectLinkOpeninNewTab);
					getCommand().waitFor(1);
					
					WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
					wait.until(ExpectedConditions.numberOfwindowsToBe(2));	
					
					ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
					
					
					if(tabs2.size()!=2)
					{
						getCommand().waitFor(3);
						
						String pageURL = getCommand().driver.getCurrentUrl();
						log("verify the Page title"+ pageURL,LogType.STEP);
                      
						getCommand().driver.get("https://preprod.us.kohler.com/us/");
						getCommand().waitFor(2);
						
					
						continue;
					
					}
					
					getCommand().driver.switchTo().window(tabs2.get(1));
					Win_size = getCommand().driver.getWindowHandles().size();
				    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
				    		
//				    String URL = footer_link.getAttribute("href");
//				    
//				    if(!URL.startsWith("https"))
//				    {
//				    	getCommand().waitFor(10);
//				    	getCommand().refreshPage();
//				    }
					String pageURL = getCommand().driver.getCurrentUrl();
					log("verify the Page title"+ pageURL,LogType.STEP);
					
					getCommand().driver.close();
					log("Close the tab and switch back to parent window",LogType.STEP);
					getCommand().driver.switchTo().window(tabs2.get(0));
				}
			}
			
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage signIn(String Data)
	{
		try
		{
			getCommand().deleteCookies();
			LoginData loginData =LoginData.fetch(Data);
			String UserName = loginData.UserName;
			String password = loginData.Password;
			log("Clicking on Sign in Button", LogType.STEP);
			getCommand().executeJavaScript("arguments[0].click();", btn_SigninHomePage);
			if(getCommand().isTargetVisible(SignInModalPopup)) {
				log("sign in modal displays", LogType.STEP);

				getCommand().clear(EmailAddress);
				log("Passing Email Address in to Email address text field", LogType.STEP);
				getCommand().sendKeys(EmailAddress, UserName);

				getCommand().clear(Password);
				log("Providing password in to password text field", LogType.STEP);
				getCommand().sendKeys(Password, password);
				JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
				js.executeScript("window.scrollBy(0,200)");
				log("Clicking on Sign in button", LogType.STEP);
				getCommand().click(btn_Signin);

				getCommand().waitFor(5);
			}
			else {
				log("sign in modal not displays", LogType.ERROR_MESSAGE);
				Assert.fail("sign in modal not displays");
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifySignIn()
	{
		try
		{
			signIn("LoginData");

			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}
			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			log("Getting Text from Username Tray", LogType.STEP);
			String UsernameText = getCommand().getText(UserNameTray);


			getCommand().executeJavaScript("arguments[0].click();", UserNameTray);

			getCommand().waitForTargetPresent(UserNameTrayDropDown);

			if(getCommand().isTargetVisible(UserNameTrayDropDown))
			{
				log("Dropdown displays after clicking on username", LogType.STEP);
				log("Navigating to My Account Page", LogType.STEP);
				
				getCommand().click(MyAccount);
			}

			else {
				log("Dropdown not display after clicking on username", LogType.ERROR_MESSAGE);
				Assert.fail("Dropdown not display after clicking on username");
			}

			log("Getting Firstname Text from My Account page", LogType.STEP);
			String FirstnameText =getCommand().getText(FirstName_MyAccount);

			String ActualText = "Hi, "+FirstnameText;



			log("Verifying name matches with logged in username", LogType.STEP);
			if(ActualText.contains(UsernameText))
			{
				log("Account is accessed and name displays as expected", LogType.STEP);
			}
			else {
				log("Account is not accessed or name displays is not as expected", LogType.ERROR_MESSAGE);
				Assert.fail("Account is not accessed or name displays is not as expected");
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifySignout() throws InterruptedException
	{
		try
		{
			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}
			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			log("Getting Text from Username Tray", LogType.STEP);
			String UsernameText = getCommand().getText(UserNameTray);		

			getCommand().executeJavaScript("arguments[0].click();", UserNameTray);

			if(getCommand().isTargetVisible(UserNameTrayDropDown))
			{
				log("Dropdown displays after clicking on username", LogType.STEP);

				log("Clicking on Signout button", LogType.STEP);

				getCommand().isTargetPresent(SignOut);

				getCommand().click(SignOut);

				getCommand().waitForTargetVisible(UserNameTray, 120);

				String UsernameTextAfterSignedout = getCommand().getText(UserNameTray);	

				if(!UsernameText.equals(UsernameTextAfterSignedout))
				{
					log("Signed out successfully", LogType.STEP);
				}

				else
				{
					log("Sign out is not working. Not able to signed out after clicking on Sign out button", LogType.STEP);
					Assert.fail("Sign out is not working. Not able to signed out after clicking on Sign out button");
				}

			}

			else {
				log("Dropdown not display after clicking on username", LogType.ERROR_MESSAGE);
				Assert.fail("Dropdown not display after clicking on username");
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyEditDelete_NotSignedIn() throws InterruptedException
	{
		try
		{
			String currentURL = getCommand().getPageUrl();
			String Text = "TestEdit";
			log("Adding items to my kohler folder",LogType.STEP);

			getCommand().isTargetPresent(AddToFolder);
			getCommand().executeJavaScript("arguments[0].click();",AddToFolder);
			
			

			getCommand().isTargetPresent(AddToFolderTextArea);
			getCommand().sendKeys(AddToFolderTextArea, Text);

			getCommand().isTargetPresent(AddToFolderSubmitButton);
			getCommand().click(AddToFolderSubmitButton);
			getCommand().waitFor(3);
			
			getCommand().isTargetPresent(AddToFolderContinueShopping);
			getCommand().click(AddToFolderContinueShopping);

			WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
			wait.until(ExpectedConditions.urlContains(currentURL));	
			
			getCommand().mouseHover(BlueBanner);
			getCommand().waitFor(3);
			
			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			getCommand().waitForTargetVisible(MyKohlerFolder, 120);

			log("Clicking on My Kohler Folder",LogType.STEP);

			getCommand().isTargetPresent(MyKohlerFolder);

			getCommand().click(MyKohlerFolder);

			getCommand().waitFor(5);

			js.executeScript("window.scrollBy(0,250)");


			List<WebElement> NoteText = getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div/div/div/div[2]/div[2]/div/p"));

			List<WebElement> Edit = getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div/div/div/div[2]/div[2]/div/a"));

			log("Editing each item and verifying note text is editable",LogType.STEP);



			int Count = 0;

			for(int i=0; i<NoteText.size();i++)
			{
				Count++;

				//Verify Edit
				js.executeScript("window.scrollBy(0,100)");
				log("Getting Note Text for "+Count+"st item",LogType.STEP);
				String NoteTextBeforeEdit = NoteText.get(i).getText();


				log("Clicking on Edit button for "+Count+"st item",LogType.STEP);
				
				WebElement EditBtn=Edit.get(i);
				js.executeScript("arguments[0].click();",EditBtn);
				
				//getCommand().executeJavaScript("arguments[0].click();",AddToFolder);
			//	Edit.get(i).click();

				getCommand().waitForTargetVisible(EditTextArea_Folder, 120);

				log("Changing text for "+Count+"st item",LogType.STEP);

				getCommand().isTargetPresent(EditTextArea_Folder);
				getCommand().sendKeys(EditTextArea_Folder, "1");

				getCommand().waitForTargetVisible(EditSave_Folder, 120);
				getCommand().isTargetPresent(EditSave_Folder);
				getCommand().click(EditSave_Folder);

				getCommand().waitFor(3);

				String NoteTextAfterEdit = NoteText.get(i).getText();

				getCommand().waitFor(3);

				log("Verifying text for "+Count+"st item after edit",LogType.STEP);
				if(!NoteTextBeforeEdit.equals(NoteTextAfterEdit) && NoteTextAfterEdit.equals(NoteTextBeforeEdit+"1"))
				{
					log("Edit is working as expected. Able to edit the text for "+Count+"st item",LogType.STEP);
				}

				else
				{
					log("Edit is not working as expected. Not Able to edit the text for "+Count+"st item",LogType.ERROR_MESSAGE);
					Assert.fail("Edit is not working as expected. Not Able to edit the text for "+Count+"st item");
				}	

			}	

			log("Verifying delete action for an item",LogType.STEP);

			//Verify Delete			
			log("Seleting all items using select all check box",LogType.STEP);
			js.executeScript("window.scrollBy(0,-500)");
			getCommand().waitForTargetVisible(SelectAllCheckBox1, 120);
			getCommand().isTargetPresent(SelectAllCheckBox1);
			getCommand().click(SelectAllCheckBox1);

			String Status = getCommand().getAttribute(Deletebtn_Folder1, "disabled");
			log("Checking delete button is enabled",LogType.STEP);
			if(Status.contains("null"))
			{
				log("Delete button is enabled",LogType.STEP);

				log("Clicking on delete button",LogType.STEP);
				getCommand().isTargetPresent(Deletebtn_Folder1);
				
				
				//getCommand().click(Deletebtn_Folder1);
				getCommand().executeJavaScript("arguments[0].click();",Deletebtn_Folder1);
				getCommand().isTargetPresent(DeleteConfirmation);
				getCommand().executeJavaScript("arguments[0].click();",DeleteConfirmation);
				//getCommand().click(DeleteConfirmation);
				getCommand().waitFor(5);

				log("Checking Items are deleted from folder",LogType.STEP);

				if(!(getCommand().isTargetVisible(MyFolderItemsGrid)))
				{
					log("Delete is working",LogType.STEP);
				}

				else
				{
					log("Delete is not working",LogType.ERROR_MESSAGE);
					Assert.fail("Delete is not working");
				}

			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifyEditDelete_SignedIn(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			String FolderText = search.FolderName1;
			
			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}
			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			
			log("Clicking on Myfolders button in Home Page",LogType.STEP);
			getCommand().waitFor(3);
			getCommand().executeJavaScript("arguments[0].click();", MyFolder);
			getCommand().waitFor(3);

			js.executeScript("window.scrollBy(0,200)");

			getCommand().click(Foldertext);

			getCommand().waitFor(5);

			js.executeScript("window.scrollBy(0,200)");

			getCommand().waitFor(3);

			List<WebElement> NoteText = getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div/div/div/div[2]/div[2]/div/p"));


			List<WebElement> Edit = getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div/div/div/div[2]/div[2]/div/a"));

			log("Editing each item and verifying note text is editable",LogType.STEP);

			int Count = 0;

			js.executeScript("window.scrollBy(0,200)");

			for(int i=0; i<NoteText.size();i++)
			{
				Count++;

				//Verify Edit
				log("Getting Note Text for "+Count+"st item",LogType.STEP);
				String NoteTextBeforeEdit = NoteText.get(i).getText();

				log("Clicking on Edit button for "+Count+"st item",LogType.STEP);
				Edit.get(i).click();

				getCommand().waitFor(3);

				log("Changing text for "+Count+"st item",LogType.STEP);

				getCommand().isTargetPresent(EditTextArea_Folder);
				getCommand().sendKeys(EditTextArea_Folder, "1");
				getCommand().waitForTargetVisible(EditSave_Folder, 120);

				getCommand().isTargetPresent(EditSave_Folder);
				getCommand().click(EditSave_Folder);

				getCommand().waitFor(3);

				String NoteTextAfterEdit = NoteText.get(i).getText();

				getCommand().waitFor(3);

				log("Verifying text for "+Count+"st item after edit",LogType.STEP);
				if(!NoteTextBeforeEdit.equals(NoteTextAfterEdit) && NoteTextAfterEdit.equals(NoteTextBeforeEdit+"1"))
				{
					log("Edit is working as expected. Able to edit the text for "+Count+"st item",LogType.STEP);
				}

				else
				{
					log("Edit is not working as expected. Not Able to edit the text for "+Count+"st item",LogType.ERROR_MESSAGE);
					Assert.fail("Edit is not working as expected. Not Able to edit the text for "+Count+"st item");
				}	

			}	

			log("Verifying delete action for an item",LogType.STEP);

			//Verify Delete			
			log("Seleting all items using select all check box",LogType.STEP);

			js.executeScript("window.scrollBy(0,-2400)");


			getCommand().isTargetPresent(SelectAllCheckBox);
			getCommand().click(SelectAllCheckBox);


			String Status = getCommand().getAttribute(Deletebtn_Folder, "disabled");
			log("Checking delete button is enabled",LogType.STEP);
			if(Status.contains("null"))
			{
				log("Delete button is enabled",LogType.STEP);

				log("Clicking on delete button",LogType.STEP);
				getCommand().isTargetPresent(Deletebtn_Folder);
				getCommand().click(Deletebtn_Folder);

				getCommand().isTargetPresent(DeleteConfirmation);
				getCommand().click(DeleteConfirmation);

				getCommand().waitFor(5);

				log("Checking Items are deleted from folder",LogType.STEP);

				if(!getCommand().isTargetVisible(MyFolderItemsGrid))
				{
					log("Delete is working",LogType.STEP);
				}

				else
				{
					log("Delete is not working",LogType.ERROR_MESSAGE);
					Assert.fail("Delete is not working");
				}

			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyMyFolderPageOptions(String Data) throws InterruptedException
	{
		try
		{
			
			SearchData search = SearchData.fetch(Data);
			String FolderText = search.FolderName1;
			String NewFolderText = search.FolderName2;
			
			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}

			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			log("Clicking on Myfolders button in Home Page",LogType.STEP);
			getCommand().waitFor(3);
			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			log("Selecting My personal folders",LogType.STEP);

			
			getCommand().waitFor(3);
			js.executeScript("window.scrollBy(0,200)");


			getCommand().click(Foldertext);


			getCommand().waitFor(5);
			//Verify Share
			log("Checking Share folder option",LogType.STEP);

			log("Clicking on Share option",LogType.STEP);

			List<WebElement> Shareoptions = getCommand().driver.findElements(By.xpath("//ul[@class='social-share-list']/li"));
			log("Clicking on each share option and checking the navigation",LogType.STEP);
	
			for(WebElement Shareoption : Shareoptions)
			{			
				getCommand().isTargetPresent(ShareFolderOption);


				getCommand().executeJavaScript("arguments[0].click();", ShareFolderOption);

				getCommand().waitFor(5);

				String Text = Shareoption.getText();

				log("Clicking on "+Text+" share option and checking the navigation",LogType.STEP);



				getCommand().waitFor(5);

				if(Text.equals("Email a Friend"))
				{

					Shareoption.click();
					String PopupText = getCommand().getText(Share_EmailAFriend);			

					String browserName = caps.getBrowserName(); 
					if (browserName.equals("MicrosoftEdge")) 
					{

						if(PopupText.equals("Email a Friend"))
						{
							log(PopupText+" window is displayed after clicking on "+Text+" share option",LogType.STEP);


							getCommand().isTargetPresent(Share_EmailAFriendClose);

							getCommand().click(Share_EmailAFriendClose);

						}

						else
						{
							log("No Window is not displayed after clicking on "+Text+" share option",LogType.ERROR_MESSAGE);
							Assert.fail("No Window is not displayed after clicking on "+Text+" share option");
						}
					}


					else
					{
						if(PopupText.equals("EMAIL A FRIEND"))
						{
							log(PopupText+" window is displayed after clicking on "+Text+" share option",LogType.STEP);


							getCommand().isTargetPresent(Share_EmailAFriendClose);

							getCommand().click(Share_EmailAFriendClose);

						}

						else
						{
							log("No Window is not displayed after clicking on "+Text+" share option",LogType.ERROR_MESSAGE);
							Assert.fail("No Window is not displayed after clicking on "+Text+" share option");
						}
					}
				}

				if(Text.equals("Share with a Showroom"))
				{
					Shareoption.click();
					String PopupText = getCommand().getText(Share_KohlerShowroom);			

					String browserName = caps.getBrowserName(); 
					if (browserName.equals("MicrosoftEdge")) 
					{

						if(PopupText.equals("Share with a Kohler Showroom"))
						{



							log(PopupText+" window is displayed after clicking on "+Text+" share option",LogType.STEP);

							getCommand().click(Share_ShareKohlerClose);
						}

						else
						{
							log("No Window is displayed after clicking on "+Text+" share option",LogType.ERROR_MESSAGE);
							Assert.fail("No Window is displayed after clicking on "+Text+" share option");
						}
					}


					else
					{


						if(PopupText.equals("SHARE WITH A KOHLER SHOWROOM"))
						{
							log(PopupText+" window is displayed after clicking on "+Text+" share option",LogType.STEP);

							getCommand().click(Share_ShareKohlerClose);
						}

						else
						{
							log("No Window is displayed after clicking on "+Text+" share option",LogType.ERROR_MESSAGE);
							Assert.fail("No Window is displayed after clicking on "+Text+" share option");
						}
					}
				}	
			}

			//Verify Copy

			log("Checking Copy folder option",LogType.STEP);

			log("Clicking on Copy option",LogType.STEP);

			List<WebElement> Items = getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div[3]/p/span"));

			getCommand().isTargetPresent(CopyFolderOption);

			getCommand().click(CopyFolderOption);

			getCommand().waitFor(5);

			log("Providing Folder Name",LogType.STEP);
			getCommand().sendKeys(FolderName1, NewFolderText);

			getCommand().isTargetPresent(SaveAsNewFolder);

			log("Cliking on save",LogType.STEP);
			getCommand().click(SaveAsNewFolder);

			log("Navigating to new folder where the item are copied",LogType.STEP);
			getCommand().mouseHover(BlueBanner);

			getCommand().executeJavaScript("arguments[0].click();", MyFolder);
			getCommand().waitFor(3);
			js.executeScript("window.scrollBy(0,200)");



			getCommand().waitFor(5);

			List<WebElement> Items_NEwFolder_Copied = getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div[3]/p/span"));


			log("Checking item are copied to new folder",LogType.STEP);

			Assert.assertNotEquals(Items_NEwFolder_Copied.size(), Items.size(), "Mismacth in items count. Copy page option is not working");

			log("Copy page option is working",LogType.STEP);

			//Verify delete

			log("Checking Delete folder option",LogType.STEP);

			getCommand().mouseHover(BlueBanner);

			getCommand().executeJavaScript("arguments[0].click();", MyFolder);
			getCommand().waitFor(3);
			js.executeScript("window.scrollBy(0,200)");



			getCommand().click(Foldertext);
			getCommand().waitFor(5);

			log("Clicking on Delete option",LogType.STEP);

			getCommand().isTargetPresent(DeleteFolderOption);

			getCommand().click(DeleteFolderOption);

			log("Clicking on Delete option from pop up",LogType.STEP);

			getCommand().isTargetPresent(DeleteFolderConfirmation);

			getCommand().click(DeleteFolderConfirmation);


			log("Navigating to My folder and checking folder is deleted",LogType.STEP);

			getCommand().mouseHover(BlueBanner);

			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			List<WebElement> FoldersName= getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/p/a"));

			for(WebElement Foldersname : FoldersName ) 
			{
				String name = Foldersname.getText();

				Assert.assertNotEquals(name, FolderText, "Delete is not working. Still deleted folder is visible under My folder");			
			}		

			log("Delete is working. Deleted folder is not visible under My folder",LogType.STEP);
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyAccountEdit() 
	{	
		try
		{
			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}
			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			
			getCommand().executeJavaScript("arguments[0].click();", UserNameTray);

			getCommand().waitForTargetVisible(UserNameTrayDropDown, 120);

			if(getCommand().isTargetVisible(UserNameTrayDropDown))
			{
				log("Dropdown displays after clicking on username", LogType.STEP);
				log("Navigating to My Account Page", LogType.STEP);
				getCommand().click(MyAccount);

				String TextbeforeEdit = getCommand().getText(ActualAccountFieldDetail);


				log("Clicking on Edit Account button in my Account Page", LogType.STEP);
				getCommand().click(EditAccount);

				getCommand().waitFor(5);

				getCommand().executeJavaScript("window.scrollBy(0,800)");


				String Text = getCommand().getAttribute(AccountFieldtoEdit, "id");

				if(!Text.contains("describe-yourself") && !Text.contains("profile-country"))
				{
					getCommand().sendKeys(AccountFieldtoEdit, "s");				
				}

				else
				{
					if(Text.contains("profile-country"))
					{

						String text = getCommand().getText(AccountFieldtoEdit);


						getCommand().click(AccountFieldtoEdit);

						List<WebElement> Options  = getCommand().driver.findElements(By.xpath("//*[@id='account-details--edit']/div[3]/div/div[6]/div[2]/div/div[2]/div[2]/ul/li"));

						for(int i=1; i<=Options.size();i++) 
						{
							String actulatext = Options.get(i).getText();
							if(!text.equals(actulatext)) {

								Options.get(i).click();
								break;
							}
						}
					}

					if(Text.contains("describe-yourself"))
					{	
	
						getCommand().click(AccountFieldtoEdit);

						List<WebElement> Options  = getCommand().driver.findElements(By.xpath("//*[@id='account-details--edit']/div[3]/div/div[9]/div[2]/div/div[2]/div[2]/ul/li"));

						for(int i=1; i<=Options.size();i++) 
						{
							String actualtext = Options.get(i).getText();

							if(actualtext.contains("Consumer/Homeowner")) 
							{
								Options.get(i).click();
								getCommand().waitFor(4);
								break;
							}
						}
					}
				}

				js.executeScript("window.scrollBy(0,-350)");
				getCommand().executeJavaScript("arguments[0].click();" ,SaveEditedAccount);

				getCommand().waitForTargetVisible(SaveSuccess, 120);
				if(getCommand().isTargetVisible(SaveSuccess))
				{
					Assert.assertEquals("Your Account Details Have Been Updated.", getCommand().getText(SaveSuccess),"Account success mesage is not as expected");
					js.executeScript("window.scrollBy(0,300)");

					String TextAfterEdit = getCommand().getText(ActualAccountFieldDetail);



					if(TextbeforeEdit!=TextAfterEdit)
					{
						log("Editing the account is working",LogType.STEP);
					}
					else
					{
						log("Editing the account is not working",LogType.ERROR_MESSAGE);
						Assert.fail("Editing the account is not working");
					}

				}

				else {
					log("Account is not updated",LogType.ERROR_MESSAGE);
					Assert.fail("Account is not updated");
				}
			}

			else {
				log("Dropdown not display after clicking on username", LogType.ERROR_MESSAGE);
				Assert.fail("Dropdown not display after clicking on username");
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyAddNewFolder(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			String Foldername = search.FolderName1;
			String FolderNote = "TestFolderCreation";
			
			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}
			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			
			log("Clicking on Myfolders button in Home Page",LogType.STEP);
			getCommand().waitFor(2);
			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			getCommand().isTargetVisibleAfterWait(CreateFolder, 120);

			if(getCommand().isTargetVisible(CreateFolder)) 
			{
				log("Able to access My Folder",LogType.STEP);
				getCommand().isTargetPresent(CreateFolder);
				getCommand().click(CreateFolder);

				log("Providing Folder Name",LogType.STEP);
				getCommand().sendKeys(FolderName, Foldername);

				log("Providing Notes",LogType.STEP);
				getCommand().sendKeys(CreateFolderNotes, FolderNote);
				js.executeScript("window.scrollBy(0,300)");

				log("Clicking on Create button",LogType.STEP);
				getCommand().isTargetPresent(btn_CreateFolder);
				getCommand().click(btn_CreateFolder);    			

				getCommand().waitFor(10);
				js.executeScript("window.scrollBy(0,200)");

				log("Checking each folder available in my folder page and clicking on newely created folder",LogType.STEP);
				List<WebElement> FoldersName= getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/p/a"));
				getCommand().waitFor(2);
				for(WebElement Foldersname : FoldersName ) 
				{
					String name = Foldersname.getText();
					if(name.equals(Foldername)) 
					{
						getCommand().waitFor(2);
						getCommand().executeJavaScript("arguments[0].click();" ,Foldertexts);

						getCommand().waitFor(2);
						log("Able to access created folder in my folder page",LogType.STEP);
						Assert.assertEquals(Foldername, getCommand().getText(ActualFolderName),"Not able to access created folder");
					}
					break;	
				}			
			}

			else 
			{
				log("My Folder is not accessable",LogType.ERROR_MESSAGE);
				Assert.fail("My Folder is not accessable");
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}

	public Kohler_HomePage VerifyPromoModuleGrid() throws InterruptedException
	{		
		try
		{
			int PromoModuleLinkCounter = 0;
			
	        List<WebElement> PromoModuleLinks = getCommand().getDriver().findElements(By.xpath("//*[@class='container' ]/ul/li[@class='promotion']/a"));
	        
	        log("Getting Total No. of Promo Module links available on the grid",LogType.STEP);
		    
		    log("Total No. of Promo Module links available on the grid are: "+PromoModuleLinks.size(),LogType.STEP);
		    
		    Actions Action = new Actions(getCommand().getDriver());
		    
		    log("Verifying Help text & access of each Promo Module Link",LogType.STEP);
		    
		    String pageurl = getCommand().getPageUrl();
		

		    for(WebElement PromoModuleLink:PromoModuleLinks)
		    {	    	
		    	JavascripExecutor(PromoModuleLink,"Y","N");    	
		    	if(PromoModuleLink.isDisplayed()) 
		    	{		    			    		
		    		String LinkText = PromoModuleLink.getAttribute("href");
		    		log("Getting Help Text for Promo Module link: "+LinkText,LogType.STEP);

		    		Action.moveToElement(PromoModuleLink).build().perform();	    		
		    		
		    		String HelText = PromoModuleLink.getAttribute("title");	    			    		
		    		log("Help Text for promotion: "+LinkText+" is, "+HelText,LogType.STEP);
		    		
		    		log("Accessing Promo Module Link: "+LinkText+" in new tab",LogType.STEP);
		    		
	                String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
	                PromoModuleLink.sendKeys(selectLinkOpeninNewTab);
	                
	                WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
					wait.until(ExpectedConditions.numberOfwindowsToBe(2));
	                ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                
	                if(listofTabs.size()>1)
    	            {
	                	 
		                log("Switching to new tab",LogType.STEP);
		                getCommand().driver.switchTo().window(listofTabs.get(1));
			    		
		                log("Getting new tab page title",LogType.STEP);
			    		String Currentpageurl = getCommand().getPageUrl();
			    	
			    		if(pageurl.equals(Currentpageurl))
			    		{	    			
			    			log("Promo Module link: "+LinkText+" is not accessable",LogType.ERROR_MESSAGE);
			    			Assert.fail("Promo Module link: "+LinkText+" is not accessable");
			    		}
			    		else
			    		{    			
			    			log("Promo Module link: "+LinkText+" is accessable",LogType.STEP); 
			    			log("Page title after accessing Promo Module link: "+LinkText+" is, "+Currentpageurl ,LogType.STEP);
			    		}
			    		
			    		getCommand().driver.close();
			    		getCommand().driver.switchTo().window(listofTabs.get(0));
			    		
			    		PromoModuleLinkCounter++;
    	            }
	                
	                else
	                {
	                	log("Getting new tab page title",LogType.STEP);
			    		String Currentpageurl= getCommand().getPageUrl();
			    	
			    		if(pageurl.equals(Currentpageurl))
			    		{	    			
			    			log("Promo Module link: "+LinkText+" is not accessable",LogType.ERROR_MESSAGE);
			    			Assert.fail("Promo Module link: "+LinkText+" is not accessable");
			    		}
			    		else
			    		{    			
			    			log("Promo Module link: "+LinkText+" is accessable",LogType.STEP); 
			    			log("Page title after accessing Promo Module link: "+LinkText+" is, "+Currentpageurl ,LogType.STEP);
			    		}

			    		PromoModuleLinkCounter++;
			    		
			    		getCommand().goBack();
	                }
	               
		    	}
		    	
		    	else
		    	{
		    		log("Promo Module link: "+PromoModuleLink.getAttribute("href")+ " is not visible",LogType.STEP);
		    	}
		    }
		    
		    if(PromoModuleLinkCounter == PromoModuleLinks.size())
		    {
		    	 log("Verification of Help text & access of each Promo Module Link is completed",LogType.STEP);
		    }
		    else
		    {
		    	log("Verification of Help text & access of each Promo Module Link is not comapleted.Mismatch in Actual promo linka and accessesd promo links",LogType.ERROR_MESSAGE);
		    }
		}
	 
	    catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyDiscoverthePossibilities_NavCircles()
	{
		try
		{
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			
			
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			
			log("Total No. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid: "+SlickDots.size(),LogType.STEP);
			ArrayList<String> list = new ArrayList<String>();
			
			log("Clicking on each Slick dots(Nav Circles) in 'Discover the Possibilities' grid",LogType.STEP);
			for(int i = 0; i < SlickDots.size(); i++)
			{			
				SlickDots.get(i).click();
				String SlickDotClass = SlickDots.get(i).getAttribute("class");
				if(SlickDotClass.contains("active"))
				{
					int index=i+1;
					String SlickDotXpath = "//*[@id='container-home']/div[3]/div/div/div[1]/ul/li["+index+"]/button";
					String SlickDotText = getCommand().getDriver().findElement(By.xpath(SlickDotXpath)).getText();
					log(SlickDotText+"st Slick dots(Nav Circles) is active and moving to next Slick dots(Nav Circles)",LogType.STEP);
					list.add(SlickDotText);		
				}
			}	
			CompareDataFromSameList(list);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage CompareDataFromSameList(List<String> list)
	{
		for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{			
				if(list.get(i).equals(list.get(k)))
				{
					Assert.fail("Mismatch in data present in the list");
				}				      
			}	      
		}		
		return this;
	}

	public Kohler_HomePage VerifyDiscoverthePossibilities_Arrows()
	{
		try
		{
			List<WebElement> CarouselSlides = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div"));
			List<Target> TargetArrowElements = new ArrayList<Target>();
			TargetArrowElements.add(NextArrow_DiscoverthePossibilities);
			TargetArrowElements.add(PrevArrow_DiscoverthePossibilities);
			
			for(Target TargetArrowElement:TargetArrowElements)
			{
				String ElementText = getCommand().getText(TargetArrowElement);
				ArrayList<String> list = new ArrayList<String>();
				int i=0;
				log("Navigating to each carouselSlide using "+ElementText+" arrow and getting the title of each carouselSlide",LogType.STEP);
				for(WebElement CarouselSlide:CarouselSlides)
				{
					String IndexPosition = CarouselSlide.getAttribute("index");
					i=i+1;
					if(IndexPosition.equals("0") || IndexPosition.equals("1") ||  IndexPosition.equals("2"))
					{
						String Xpath = "//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+i+"]/div[1]";
						String Title = getCommand().getDriver().findElement(By.xpath(Xpath)).getAttribute("title");
						
						log("Title of the carouselSlide is: "+Title,LogType.STEP);
						list.add(Title);
						log("Clicking on "+ElementText+" arrow",LogType.STEP);	
						getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", TargetArrowElement);
						getCommand().waitForTargetPresent(TargetArrowElement);
						getCommand().click(TargetArrowElement);
					}
				}
				CompareDataFromSameList(list);
				log("Navigating to each carouselSlide using "+ElementText+" arrow is Working",LogType.STEP);
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyDiscoverthePossibilities_LearnMore()
	{
		try
		{
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Clicking on Lear more CTA available in each carousel slide of 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> LearnMore = getCommand().getDriver().findElements(By.xpath("//*[@class='carousel-cta__right']/a"));
			int i=0;
			for(WebElement Learnmore:LearnMore)
			{			
				if(Learnmore.isDisplayed())
				{
					String href = Learnmore.getAttribute("href");
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
					log("Opening Learn more CTA with link: "+href+" in new tab",LogType.STEP);
					Learnmore.sendKeys(selectLinkOpeninNewTab);
					getCommand().waitFor(5);
	                ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                log("Switching to new tab",LogType.STEP);
	                getCommand().driver.switchTo().window(listofTabs.get(1));    		
	                log("Getting new tab page url",LogType.STEP);
		    		String Currentpageurl = getCommand().driver.getCurrentUrl();
		    		String[] parts = Currentpageurl.split("\\/");
		    		String ExpectedPartofpageurl = parts[2];    
		    	
		    		if(ExpectedPartofpageurl.equals("ideas.kohler.com"))
		    		{	    			
		    			log("Learn More CTA opens a ideas.kohler.com page",LogType.STEP);
		    		}
		    		else
		    		{    	
		    			log("Page url after clicking on learn more CTA is: , "+Currentpageurl ,LogType.STEP);
		    			log("Learn More CTA not opens a ideas.kohler.com page",LogType.ERROR_MESSAGE); 
		    			Assert.fail("Learn More CTA not opens a ideas.kohler.com page");
		    			
		    		}
		    		
		    		getCommand().driver.close();
		    		getCommand().driver.switchTo().window(listofTabs.get(0));
		    		log("Moving to next slide of 'Discover the Possibilities' grid",LogType.STEP);
		    		i=i+1;
				}
				
				if(i<SlickDots.size())
				{
					Actions action = new Actions(getCommand().driver);
					action.click(SlickDots.get(i)).build().perform();
					//SlickDots.get(i).click();
					getCommand().waitFor(5);
				}
				
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyDiscoverthePossibilities_HotSpots_GetDetails() throws InterruptedException
	{
		try
		{
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li/button"));	
			int i=2;
			int j=2;
			int k=2;
			for(WebElement SlickDot : SlickDots)
			{
				js.executeScript("arguments[0].click();", SlickDot);   
				log("Getting Total no. of hot spots available in the slide",LogType.STEP);
				List<WebElement> HotSpots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+i+"]/div[1]/div/div"));
				int HotSpotsCount = HotSpots.size();
				log("Total " +HotSpotsCount+" hot spots available in the slide",LogType.STEP);
				log("Clicking on each hot spot",LogType.STEP);
				for(WebElement HotSpot : HotSpots) 
				{
					getCommand().waitFor(2);
					js.executeScript("arguments[0].click();", HotSpot);
					String GetDetailsXpath = "//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+k+"]/div["+j+"]/div/p[2]/a";
					WebElement GetDetails = getCommand().driver.findElement(By.xpath(GetDetailsXpath));
					getCommand().waitFor(2);
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
					log("Opening get details link in new tab ",LogType.STEP);
					GetDetails.sendKeys(selectLinkOpeninNewTab);
					getCommand().waitFor(2);
					ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
					if(listofTabs.size()!=2)
					{
						getCommand().waitFor(2);
						
						String pageTitle = getCommand().driver.getTitle();
						log("verify the Page title"+ pageTitle,LogType.STEP);
						getCommand().goBack();
                      
						/*getCommand().driver.get("https://preprod.us.kohler.com/us/");
						getCommand().waitFor(2);
						List<WebElement> HotSpots1 = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+i+"]/div[1]/div/div"));

						
						continue;*/
					
					}
	                log("Switching to new tab",LogType.STEP);
	                getCommand().waitFor(2);
	                getCommand().driver.switchTo().window(listofTabs.get(1));    		
	                log("Getting new tab page url",LogType.STEP);                
		    		String Currentpageurl = getCommand().driver.getCurrentUrl();
		    		
		    		if(Currentpageurl.contains("productDetail")) {
		    			log("Product page is displayed",LogType.STEP);
		    			
		    		}
		    		else
		    		{
		    			log("product page is not displayed",LogType.ERROR_MESSAGE);
		    			Assert.fail("product page is not displayed");
		    		}
		    		j++;
		    		getCommand().driver.close();
		    		getCommand().driver.switchTo().window(listofTabs.get(0));  	
		    		getCommand().waitFor(4);
				}
				j=2;
				k++;
				i++;
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyDiscoverthePossibilities_HotSpots_StoreLocator() throws InterruptedException
	{
		try
		{
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));	
			int i=2;
			int j=2;
			int k=2;
			for(int n=1;n<SlickDots.size();n++)		
			{
				List<WebElement> slickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li/button[1]"));
				Actions action = new Actions(getCommand().driver);
				action.click(SlickDots.get(n)).build().perform();
				//js.executeScript("arguments[0].click();", slickDots.get(n));
				//slickDots.get(n).click();			
				log("Getting Total no. of hot spots available in the slide",LogType.STEP);
				List<WebElement> HotSpots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+i+"]/div[1]/div/div"));
				int HotSpotsCount = HotSpots.size();
				log("Total " +HotSpotsCount+" hot spots available in the slide",LogType.STEP);
				log("Clicking on each hot spot",LogType.STEP);
				for(int m=0;m<HotSpots.size();m++) 
				{
					List<WebElement> Slickdots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
					//js.executeScript("arguments[0].click();", Slickdots.get(n));
					action.click(Slickdots.get(n)).build().perform();
					//Slickdots.get(n).click();
					List<WebElement> Hotspots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+i+"]/div[1]/div/div"));
					getCommand().waitFor(2);
					js.executeScript("arguments[0].click();", Hotspots.get(m));
					String GetDetailsXpath = "//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+k+"]/div["+j+"]/div/a";
					WebElement GetDetails = getCommand().driver.findElement(By.xpath(GetDetailsXpath));
					getCommand().waitFor(3);
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
					log("Opening Find a store link in new tab ",LogType.STEP);
					getCommand().waitFor(2);
					GetDetails.sendKeys(selectLinkOpeninNewTab);
					ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
					
					if(listofTabs.size()>1)
					{
						log("Switching to new tab",LogType.STEP);
		                getCommand().driver.switchTo().window(listofTabs.get(1));    
		                getCommand().waitForTargetVisible(FindaStore, 120);
		                log("Getting new tab page url",LogType.STEP);                
			    		String Currentpageurl = getCommand().driver.getCurrentUrl();
			    		
			    		if(Currentpageurl.contains("storelocator")) {
			    			log("Store locator page is displayed",LogType.STEP);
			    		}
			    		else
			    		{
			    			log("Store locator page is not displayed",LogType.ERROR_MESSAGE);
			    			Assert.fail("Store locator page is not displayed");
			    		}
			    		j++;
			    		getCommand().driver.close();
			    		getCommand().driver.switchTo().window(listofTabs.get(0));  	
			    		getCommand().waitFor(2);
					
					}
					
					else
					{
						getCommand().waitForTargetVisible(FindaStore, 120);						
						String pageurl = getCommand().driver.getCurrentUrl();
                      
						if(pageurl.contains("storelocator")) {
			    			log("Store locator page is displayed",LogType.STEP);
			    		}
			    		else
			    		{
			    			log("Store locator page is not displayed",LogType.ERROR_MESSAGE);
			    			Assert.fail("Store locator page is not displayed");
			    		}
			    		j++;
						
						getCommand().goBack();
						getCommand().waitFor(4);
					}
				}
				j=2;
				k++;
				i++;
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	public Kohler_HomePage VerifyDiscoverthePossibilities_Share()
	{
		try
		{
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Clicking on Share CTA available in each carousel slide of 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> Share = getCommand().getDriver().findElements(By.xpath("//*[@class='carousel-cta__right']/button[2]"));
			List<String> ActualSocialmedia = new ArrayList<String>();
			int i=0;
			int j=0;
			int ExpectedSocialMediaCount = 5;
			for(WebElement share:Share)
			{
				int ActualSocialMediaCount = 0;
				ActualSocialmedia.clear();
				if(share.isDisplayed())
				{
					j++;
					share.click();
	                List<WebElement> ShareList = getCommand().driver.findElements(By.xpath("//*[@class ='share-tip-inner']/ul/li"));
		    		
		    		for(WebElement Sharelist : ShareList) {
		    			String SocialSiteText = Sharelist.getText();
		    			if(Sharelist.isDisplayed() && !SocialSiteText.isEmpty()) 
		    			{
		    				ActualSocialmedia.add(SocialSiteText);
		    				log(SocialSiteText+ " is displayed in share CTA of "+j+" carousel slide in 'Discover the Possibilities' grid",LogType.STEP);
		    			}
		    			
		    			else 
		    			{
		    				log(SocialSiteText+ " is not displayed in share CTA of "+j+" carousel slide in 'Discover the Possibilities' grid",LogType.STEP);
		    			}
		    		}
		    		
		    		for(String Actualsocialmedia : ActualSocialmedia)
		    		{
		    			if(Actualsocialmedia.equals("Pinterest") || Actualsocialmedia.equals("Houzz") || Actualsocialmedia.equals("Facebook") || Actualsocialmedia.equals("Google+") || Actualsocialmedia.equals("Email a friend") || Actualsocialmedia.equals("Twitter") || Actualsocialmedia.equals("Share with a Kohler Showroom"))
		    			{
		    				ActualSocialMediaCount++;	    				
		    			}
		    		}
		    		
		    		Assert.assertEquals(ExpectedSocialMediaCount, ActualSocialMediaCount, "Mismatch in media Count to be present in share CTA drop sown");
		    		
		    		i=i+1;
				}
				
				if(i<SlickDots.size())
				{
					Actions action = new Actions(getCommand().driver);
					action.click(SlickDots.get(i)).build().perform();
					getCommand().waitFor(5);
				}
				
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyDiscoverthePossibilities_AddToFolder() throws InterruptedException
	{
		try
		{
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Clicking on Add to Folder CTA available in each carousel slide of 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> AddtoFolder = getCommand().getDriver().findElements(By.xpath("//*[@class ='carousel-cta__right']/button[@data-params='addToFolder,relative,300']"));
			List<String> ActualNoteText = new ArrayList<String>();
			int i=0;
			int j=0;
			int k=1;
			String Text = "Test";
			for(WebElement Addtofolder:AddtoFolder)
			{
				if(Addtofolder.isDisplayed()) {
					log("Clicking on Save to folder CTA of "+k+" carousel slide in 'Discover the Possibilities' grid",LogType.STEP);
					JavascripExecutor(Addtofolder,"N","Y");
					j=j+1;
					getCommand().waitFor(3);
					log("Adding Note and Saving to My folder",LogType.STEP);
					getCommand().isTargetPresent(AddToFolderTextArea);
					getCommand().sendKeys(AddToFolderTextArea, Text+j);

                    getCommand().isTargetPresent(AddToFolderSubmitButton);
                    getCommand().executeJavaScript("arguments[0].click();", AddToFolderSubmitButton);

					log("Clicking on Continue shopping and navigating to next slide",LogType.STEP);

					getCommand().isTargetPresent(AddToFolderContinueShopping);
                    getCommand().executeJavaScript("arguments[0].click();", AddToFolderContinueShopping);
					i++;
					k++;
				}

				if(i<SlickDots.size())
				{
					JavascriptExecutor js = (JavascriptExecutor)getCommand().driver;
					js.executeScript("arguments[0].click();", SlickDots.get(i));
					getCommand().waitFor(5);
				}

			}		
			getCommand().mouseHover(BlueBanner);

			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			getCommand().waitFor(5);

			getCommand().isTargetPresent(MyKohlerFolder);

			getCommand().click(MyKohlerFolder);

			getCommand().waitFor(5);

			List<WebElement> NoteText = getCommand().driver.findElements(By.xpath("//*[@class='add_to_cart_folder']/div/div[2]/div[2]/div/p/span[2]"));

			for(WebElement Notetext : NoteText) {
				ActualNoteText.add(Notetext.getText());
			}
			Set<String> Set = new LinkedHashSet<>(ActualNoteText);

			for(String set : Set)
			{
				if(set.equals("Test1") || set.equals("Test2") || set.equals("Test3")) {
					log(set+ " is added to myfolder",LogType.STEP);
				}
				else {
					log(set+ " is not added to myfolder",LogType.ERROR_MESSAGE);
				}
			}		
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}


	public Kohler_HomePage VerifyDiscoverthePossibilities_AddToFolderSignin() throws InterruptedException
	{
		String browserName = caps.getBrowserName();
		try
		{
			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}
			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			
			getCommand().waitForTargetVisible(NextArrow_DiscoverthePossibilities, 120);
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> SlickDots = getCommand().driver.findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Clicking on Add to Folder CTA available in each carousel slide of 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> AddtoFolder = getCommand().driver.findElements(By.xpath("//*[@class ='carousel-cta__right']/button[@data-params='addToFolder,relative,300']"));
			List<String> ActualNoteText = new ArrayList<String>();
			int i=0;
			int j=0;
			int k=1;
			String Text = "Test";
			String FolderText = "TestFolderCreation";
			for(WebElement Addtofolder:AddtoFolder)
			{
				if(Addtofolder.isDisplayed()) {
					log("Clicking on Save to folder CTA of "+k+"st carousel slide in 'Discover the Possibilities' grid",LogType.STEP);
					JavascripExecutor(Addtofolder,"N","Y");
					getCommand().waitForTargetVisible(AddToFolderDropDown, 120);
					getCommand().executeJavaScript("arguments[0].click();", AddToFolderDropDown);

					List<WebElement> Options = getCommand().driver.findElements(By.xpath("//*[@id='folderDropdown_msdd']/div[2]/ul/li/span"));

					for(WebElement Option : Options) {
						String dropdownText = Option.getText();                            
						if(FolderText.equals(dropdownText)) 
						{
							log("Selecting the folder from drop down",LogType.STEP);
							Option.click();
							break;
						}                                        
					}

					j=j+1;
					log("Adding Note and Saving to selected folder",LogType.STEP);
					getCommand().isTargetPresent(AddToFolderTextArea_Signin);
					getCommand().sendKeys(AddToFolderTextArea_Signin, Text+j);

					getCommand().isTargetPresent(AddToFolderSubmitButton);
					getCommand().executeJavaScript("arguments[0].click();", AddToFolderSubmitButton);
					getCommand().waitFor(3);

					log("Clicking on Continue shopping and navigating to next slide",LogType.STEP);

					getCommand().isTargetPresent(AddToFolderContinueShopping);
					getCommand().executeJavaScript("arguments[0].click();", AddToFolderContinueShopping);
					i++;
					k++;
				}

				if(i<SlickDots.size())
				{
					SlickDots.get(i).click();
					getCommand().waitFor(5);
				}

			}   
			
			getCommand().mouseHover(BlueBanner);

			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			getCommand().waitForTargetVisible(MyFolderHeader, 120);

			List<WebElement> FoldersName= getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/p[@class='my-folders-tile__description']/a"));

			for(WebElement Foldersname : FoldersName ) 
			{
				String name = Foldersname.getText();
				if(browserName.equals("MicrosoftEdge"))
				{
					name = name.substring(0, 18);
				}

				if(name.equals(FolderText)) 
				{
					JavascripExecutor(Foldersname,"N","Y");       
					getCommand().waitForTargetVisible(MyFolderImage, 120);
					String FOlderText = getCommand().getText(ActualFolderName);
					if(browserName.equals("MicrosoftEdge"))
					{
						FOlderText = FOlderText.substring(0, 18);
					}
					Assert.assertEquals(FolderText, FOlderText,"Mismatch in foldername which was accessed");
					log("Able to access created folder in my folder page",LogType.STEP);
					break;
				}                                 
			}

			List<WebElement> NoteText = getCommand().driver.findElements(By.xpath("//*[@class='add_to_cart_folder']/div/div[2]/div[2]/div/p/span[2]"));

			for(WebElement Notetext : NoteText) {
				ActualNoteText.add(Notetext.getText());
			}
			Set<String> Set = new LinkedHashSet<>(ActualNoteText);

			for(String set : Set)
			{
				if(set.equals("Test1") || set.equals("Test2") || set.equals("Test3")) {
					log(set+ " is added to myfolder",LogType.STEP);
				}
				else {
					log(set+ " is not added to myfolder",LogType.ERROR_MESSAGE);
					Assert.fail(set+ " is not added to myfolder");
				}
			}

			log("Checking Folder layout", LogType.STEP);
			Assert.assertTrue(getCommand().isTargetVisible(MyFolderImage),"Folder Image is not present");
			log("Folder Image is present", LogType.STEP);
			Assert.assertTrue(getCommand().isTargetVisible(MyFolderCost),"Cost is not present");
			log("Cost is present", LogType.STEP);
			Assert.assertTrue(getCommand().isTargetVisible(MyFolderNotes),"Notes is not present");
			log("Notes is present", LogType.STEP);
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}


	public Kohler_HomePage VerifyMyFoldersCopyAction_SignedIn(String Data) throws InterruptedException
	{	
		try
		{
			SearchData search = SearchData.fetch(Data);
			String FolderText = search.FolderName1;
			String Foldername = search.FolderName2;
			String FolderNote = "TestFolderCreation";
			log("Clicking on Myfolders button in Home Page",LogType.STEP);

			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}
			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			
			getCommand().waitForTargetVisible(MyFolder, 120);

			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			getCommand().waitFor(3);
			js.executeScript("window.scrollBy(0,200)");

			if(getCommand().isTargetVisible(CreateFolder)) 
			{
				log("Able to access My Folder",LogType.STEP);
				log("Clicking on Create folder in My Folder Page",LogType.STEP);
				getCommand().isTargetPresent(CreateFolder);
				getCommand().click(CreateFolder);

				log("Providing Folder Name",LogType.STEP);
				getCommand().sendKeys(FolderName, Foldername);

				log("Providing Notes",LogType.STEP);
				getCommand().sendKeys(CreateFolderNotes, FolderNote);


				log("Clicking on Create button",LogType.STEP);
				getCommand().waitFor(3);
				//getCommand().click(btn_CreateFolder);
//				JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
//				js.executeScript("window.scrollBy(0,200)");
//				
//				WebDriverWait wait = new WebDriverWait(getCommand().driver,30);
//				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='createNewFolder']//button[1]")));	
//				
//				getCommand().executeJavaScript("arguments[0].click();", btn_CreateFolder);
//				
				WebElement tmpElement= getCommand().driver.findElement(By.xpath("//*[@id='createNewFolder']//button[@class='btn btn--primary btn--blue']"));
				JavascriptExecutor executor = (JavascriptExecutor)getCommand().driver;
				executor.executeScript("arguments[0].scrollIntoView(true);", tmpElement);
				getCommand().waitFor(3);
				executor.executeScript("arguments[0].click();", tmpElement);

				getCommand().waitFor(3);
			}

			js.executeScript("window.scrollBy(0,200)");
			WebElement create_button = getCommand().driver.findElement(By.linkText("CREATE "));
			js.executeScript("arguments[0].click();", create_button);
			//getCommand().click(Foldertext);

			getCommand().waitFor(3);

			js.executeScript("window.scrollBy(0,200)");

			List<WebElement> Items = getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/div/a"));

			log("Select an item using check box",LogType.STEP);



			getCommand().isTargetPresent(CheckBox1);

			getCommand().click(CheckBox1);

			getCommand().isTargetPresent(CopyMyFolder_Signedin);

			getCommand().click(CopyMyFolder_Signedin);


			getCommand().executeJavaScript("arguments[0].click();", AddToFolderDropDown);

			List<WebElement> Options = getCommand().driver.findElements(By.xpath("//*[@id='folderDropdown_msdd']/div[2]/ul/li/span"));

			for(WebElement Option : Options) {
				String dropdownText = Option.getText();					
				if(Foldername.equals(dropdownText)) 
				{
					log("Selecting the folder from drop down",LogType.STEP);
					Option.click();
					break;
				}			
			}

			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);",Add_CopyItem);
			getCommand().executeJavaScript("arguments[0].click();", Add_CopyItem);

			getCommand().mouseHover(BlueBanner);

			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			getCommand().waitFor(3);

			js.executeScript("window.scrollBy(0,200)");


			getCommand().click(foldername);

			getCommand().waitFor(3);
			js.executeScript("window.scrollBy(0,200)");

	

			List<WebElement> Items_NEwFolder_Copied = getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/div/a"));

			Assert.assertEquals(Items_NEwFolder_Copied.size(), Items.size(), "Copy is not working.");

			log("Copy is working",LogType.STEP);
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}

	public Kohler_HomePage VerifyMyFoldersMoveAction_SignedIn(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			String FolderText = search.FolderName1;
			String Foldername = search.FolderName2;
			
			String value=null;
			if(getCommand().isTargetVisible(SignUpForEmail))
			{
				value = getCommand().getText(SignUpForEmail);			
		 	}
			
			if(value == null)
			{
				log("Alert is not present on HomePage",LogType.STEP);
			}
			else{
				log("Alert is present on home page",LogType.STEP);
				getCommand().click(SignUpForEmailClose);
			}
			
			
			log("Clicking on Myfolders button in Home Page",LogType.STEP);
			getCommand().waitFor(3);
			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			getCommand().waitFor(3);


			js.executeScript("window.scrollBy(0,200)");
			List<WebElement> Items = getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div[3]/p/span"));
	

			getCommand().click(Foldertexts);

			getCommand().waitFor(3);
			log("Select an item using check box",LogType.STEP);

			js.executeScript("window.scrollBy(0,200)");
			String itemtext = getCommand().getText(SecondItemText);



			getCommand().waitFor(3);

			js.executeScript("window.scrollBy(0,300)");

			getCommand().isTargetPresent(CheckBox1);

			getCommand().click(CheckBox1);

			getCommand().mouseHover(MoveMyFolder_Signedin);

			getCommand().isTargetPresent(MoveMyFolder_Signedin);

			getCommand().click(MoveMyFolder_Signedin);


			getCommand().executeJavaScript("arguments[0].click();", AddToFolderDropDown);

			List<WebElement> Options = getCommand().driver.findElements(By.xpath("//*[@id='folderDropdown_msdd']/div[2]/ul/li/span"));

			for(WebElement Option : Options) {
				String dropdownText = Option.getText();					
				if(Foldername.equals(dropdownText)) 
				{
					log("Selecting the folder from drop down",LogType.STEP);
					Option.click();
					break;
				}			
			}

			getCommand().isTargetPresent(Add_CopyItem);

			getCommand().click(Add_CopyItem);

			getCommand().waitFor(4);

			getCommand().mouseHover(BlueBanner);

			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			getCommand().waitFor(5);

			List<WebElement> ItemsAfterMoved = getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div[3]/p/span"));

			Assert.assertEquals(Items.size(), ItemsAfterMoved.size(),"Item from Main folder is not removed after move action");


			getCommand().mouseHover(BlueBanner);

			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			getCommand().waitFor(3);

			js.executeScript("window.scrollBy(0,300)");

			getCommand().click(TestFolderCreation1);


			getCommand().waitFor(3);

			js.executeScript("window.scrollBy(0,300)");

			String itemtextfromMovedFolder = getCommand().getText(SecondItemText);


			if(itemtextfromMovedFolder.equals(itemtext)) 
			{
				log("Move is  working",LogType.STEP);

			}
			else
			{
				log("Move is NOT working",LogType.ERROR_MESSAGE);
				Assert.fail("Move is NOT working");
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}

	public Kohler_HomePage SelectFolder(String FolderText) throws InterruptedException
	{
		try
		{
			List<WebElement> FoldersName= getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/p/a"));

			for(WebElement Foldersname : FoldersName ) 
			{
				String name = Foldersname.getText();
				if(name.equals(FolderText)) 
				{
					Foldersname.click();				
					Assert.assertEquals(FolderText, getCommand().getText(ActualFolderName));
					log("Able to access created folder with items in my folder page",LogType.STEP);
					break;
				}
			}		

			getCommand().waitFor(3);
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyKohlerIdeaslayout_PromoImages()
	{
		try
		{
			log("Verifying Count of images in #KohlerIdeas layout",LogType.STEP);
		    
		    int ImageCounter = 0;
		    int ExpectedImageCount = 5;
		    
		    List<WebElement> listImages = getCommand().getDriver().findElements(By.className("curalate-thumbnail"));
		   
		    
		    log("Total No. of Images exist in #KohlerIdeas layout: "+listImages.size(),LogType.STEP);
		    
		    for(WebElement Images:listImages)
		    {
		    	if(Images.isDisplayed()) 
		    	{
		    		ImageCounter++;
		    		if(ImageCounter==5)
		    		{
		    			break;
		    		}
		    	}   	
		    }

		    Assert.assertEquals(ImageCounter, ExpectedImageCount, "Images displayed in #KohlerIdeas layout is not as expected count");
		    
		    log("Total No. of Images displayed in #KohlerIdeas layout: "+ImageCounter,LogType.STEP);
		}
			    
	    catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}

	public Kohler_HomePage VerifyKohlerIdeaslayout_Arrows()
	{		
		try
		{
			js.executeScript("window.scrollBy(0,2500)");
			log("Verifying Only right arrow is displayed by default in #KohlerIdeas layout",LogType.STEP);
			
			getCommand().waitForTargetVisible(KohlerIdeaslayout_RightArrow, 120);
			
			//getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", KohlerIdeaslayout_RightArrow);
			
			if(!getCommand().isTargetVisible(KohlerIdeaslayout_LeftArrow))
			{
				if(getCommand().isTargetVisible(KohlerIdeaslayout_RightArrow))
				{
				   log("By default Only right arrow is displayed in #KohlerIdeas layout",LogType.STEP);
				}						
			}
			
			else
			{
				log("Left Arrow is displayed by default in #KohlerIdeas layout",LogType.ERROR_MESSAGE);
				Assert.fail("Left Arrow is displayed by default in #KohlerIdeas layout");
			}
			
			log("Verifying Left arrow is displayed after moving carousel to right by clicking right arrow in #KohlerIdeas layout",LogType.STEP);
			
			getCommand().mouseHover(KohlerIdeaslayout_RightArrow);

			getCommand().executeJavaScript("arguments[0].click();", KohlerIdeaslayout_RightArrow);
					
			if(getCommand().isTargetVisible(KohlerIdeaslayout_LeftArrow))
			{
				log("Left arrow is displayed after moving carousel to right by clicking right arrow in #KohlerIdeas layout",LogType.STEP);			
			}
			
			else
			{
				log("Left Arrow is not displayed even after moving carousel to right in #KohlerIdeas layout",LogType.ERROR_MESSAGE);	
				Assert.fail("Left Arrow is not displayed even after moving carousel to right in #KohlerIdeas layout");
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyKohlerIdeaslayout_ButonsAndLinks()
	{
		try
		{
			WebElement button = getCommand().driver.findElement(By.id("curalate-upload-photos"));
			
			JavascriptExecutor js = (JavascriptExecutor)getCommand().driver;
			js.executeScript("arguments[0].scrollIntoView(true);", button);
			
			
			getCommand().waitForTargetPresent(KohlerIdeaslayout_ViewGallery);
			getCommand().isTargetPresent(KohlerIdeaslayout_ViewGallery);
			log("View Gallery is present",LogType.STEP);
			getCommand().waitForTargetPresent(KohlerIdeaslayout_SubmitaPhoto);
			getCommand().isTargetPresent(KohlerIdeaslayout_SubmitaPhoto);
			log("Submit a Photo is present",LogType.STEP);
			
			js.executeScript("arguments[0].click();", button);

			getCommand().driver.switchTo().frame(getCommand().driver.findElement(By.id("curalate-photo-picker")));
			log("multi-step uploader modal displays",LogType.STEP);
			getCommand().waitForTargetPresent(KohlerIdeaslayout_Uploader);
			getCommand().isTargetPresent(KohlerIdeaslayout_Uploader);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	public Kohler_HomePage VerifyKohlerIdeaslayout_ToolTip()
	{
		try
		{		  
			js.executeScript("window.scrollBy(0,2500)");
		    WebElement Tootip = getCommand().driver.findElement(By.className("curalate-hover-username"));
		  
		    List<WebElement> listImages = getCommand().getDriver().findElements(By.className("curalate-thumbnail"));

		    WebElement NextButton = getCommand().getDriver().findElement(By.xpath("//*[@id='curalate-content']/button[2]"));
		    
		    log("Total No. of Images exist in #KohlerIdeas layout: "+listImages.size(),LogType.STEP);

		    for(WebElement Images:listImages)
		    {
		    	
		    	String id = Images.getAttribute("id");
				char ch='"';
				String Xpath_ToolTip = "//*[@id="+ch+id+ch+"]/div/div/div/div";	
		    	
		    	if(Images.isDisplayed()) 
		    	{
		    		if(!Tootip.isDisplayed())
	    		    {
					    Action.moveToElement(Images).perform();
					    if(getCommand().driver.findElement(By.xpath(Xpath_ToolTip)).isDisplayed())
					    {
					    	String TooltipText = getCommand().driver.findElement(By.xpath(Xpath_ToolTip)).getText();
					    	int length = TooltipText.length();
					    	String tooltiptext = TooltipText.substring(1, length);
					    	if(TooltipText.contains("@") && TooltipText.equals("@"+tooltiptext))
					    	{
					    		log("@"+tooltiptext+" displays on hovering on image.",LogType.STEP);
					    	}
					    	else
					    	{
					    		log("Tooltip text is not as expected format on hovering on image. Actual: "+TooltipText,LogType.ERROR_MESSAGE);
					    		Assert.fail("Tooltip text is not as expected format on hovering on image. Actual: "+TooltipText);
					    	}
						    
					    }			    
					    Action.moveToElement(NextButton).perform();		
		    		}	    	   
		    	}	
		    	
		    	if(!Images.isDisplayed()) 
		    	{
		    		NextButton.click();
		    		if(!Tootip.isDisplayed())
	    		    {
		    			Action.moveToElement(Images).perform();
					    if(getCommand().driver.findElement(By.xpath(Xpath_ToolTip)).isDisplayed())
					    {
					    	String TooltipText = getCommand().driver.findElement(By.xpath(Xpath_ToolTip)).getText();
					    	int length = TooltipText.length();
					    	String tooltiptext = TooltipText.substring(1, length);
					    	if(TooltipText.contains("@") && TooltipText.equals("@"+tooltiptext))
					    	{
					    		log("@"+tooltiptext+" displays on hovering on image.",LogType.STEP);
					    	}
					    	else
					    	{
					    		log("Tooltip text is not as expected format on hovering on image. Actual: "+TooltipText,LogType.ERROR_MESSAGE);
					    		Assert.fail("Tooltip text is not as expected format on hovering on image. Actual: "+TooltipText);
					    	}
						    
					    }			    
					    Action.moveToElement(NextButton).perform();			    		    			    
		    		}	    	   
		    	}
		    }
		}

	    catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
	    
		return this;
	}

	public Kohler_HomePage VerifyViewGalleryScreen()
	{
		log("Verify View Gallery button is visible",LogType.STEP);
		WebElement ViewGallery=getCommand().driver.findElement(By.xpath("//a[@class='curalate-view-gallery']"));
		if(ViewGallery.isDisplayed())
		{
			log("Click on View Gallery button",LogType.STEP);
			JavascriptExecutor js = (JavascriptExecutor)getCommand().driver;
			js.executeScript("arguments[0].click();", ViewGallery);
			
			getCommand().waitForTargetVisible(GalleryPageTitle);
				
			log(getCommand().getText(GalleryPageTitle)+" is displayed",LogType.STEP);
		}
		
		return this;
	}

	public Kohler_HomePage VerifyKohlerIdeas_OverlayDisplay()
	{
		try
		{
			String Backgroundcolor= "rgba(100, 100, 100, 0.8)";
		    
			String Style= "block";
			
	        getCommand().waitForTargetVisible(KohlerImage, 120);
		    
		    log("Clicking on image",LogType.STEP);
		    
		    if(getCommand().isTargetVisible(KohlerImage)) 		    		
	    	{		    		
		    	getCommand().click(KohlerImage);
	    		getCommand().waitFor(3);		    		
	    		WebElement OverlayDisplay = getCommand().driver.findElement(By.xpath("//div[@class='curalate-overlay active']"));			    		
	    		CheckOverlayDisplay(OverlayDisplay,Backgroundcolor,Style);	    		
	    	}
		    
		    else
		    {
		    	 log("Image is not visible",LogType.STEP);
		    }
		
		}
	    catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
	    
		return this;
	}

	public Kohler_HomePage CheckOverlayDisplay(WebElement Element, String BackGroundColor, String ElementStyle)
	{
		try
		{
			String Backgroundcolor= Element.getCssValue("background-color");
			String Style= Element.getAttribute("style");

			if(Backgroundcolor.equals(BackGroundColor) && Style.contains(ElementStyle))
			{
				log("Overlay Displays after clicking on image in #KohlerIdeas layout",LogType.STEP);    		    
			}

			else
			{
				log("Overlay popup is not Displayed after clicking on image in #KohlerIdeas layout",LogType.ERROR_MESSAGE);
				Assert.fail("Overlay popup is not Displayed after clicking on image in #KohlerIdeas layout");
			}
			GetOverlayDisplayContent();
			CheckShareElementsFunctionality_OverlayDisplay();
			CheckCrossElementsFunctionality_OverlayDisplay();	
			String PopupStyle= Element.getAttribute("style");
			if(!PopupStyle.contains("block"))
			{
				log("Overlay popup dissapear after clicking on cross in Overlay Display of KohlerIdeas",LogType.STEP);			
			}
			
			else
			{
				log("Overlay popup not dissapear after clicking on cross in Overlay Display of KohlerIdeas",LogType.ERROR_MESSAGE);	
				Assert.fail("Overlay popup not dissapear after clicking on cross in Overlay Display of KohlerIdeas");
			}
		} 	

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}

	public Kohler_HomePage GetOverlayDisplayContent() 
	{
		try
		{
			List<Target> TargetElements = new ArrayList<Target>();
			TargetElements.add(OverlayDisplayCross);
			TargetElements.add(OverlayDisplay_RightArrow);
			TargetElements.add(OverlayDisplay_UserName);
			TargetElements.add(OverlayDisplay_Share);
			TargetElements.add(OverlayDisplay_Share_F);
			TargetElements.add(OverlayDisplay_Share_T);
			TargetElements.add(OverlayDisplay_Share_P);
			TargetElements.add(OverlayDisplay_ShopHeader);		
			TargetElements.add(OverlayDisplay_SmallProductImage);
			TargetElements.add(OverlayDisplay_SmallProductDescription);
			TargetElements.add(OverlayDisplay_SmallProductNxt);
			TargetElements.add(OverlayDisplay_SmallProductPrev);

			CheckElementsVisibility(TargetElements,"Y","Y",OverlayDisplay_LeftArrow,OverlayDisplay_RightArrow);
			log("Target Element's are visible",LogType.STEP);		
		}
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}

	public Kohler_HomePage CheckElementsVisibility(List<Target> TargetElements,String IsCheckMultipleElements, String IsCheckLeftArrow, Target LeftElement, Target RightElement)
	{
		try
		{
			if(IsCheckMultipleElements=="Y") 
			{
				for(Target Target:TargetElements)
				{
					getCommand().waitForTargetPresent(Target);
					if(!getCommand().isTargetPresent(Target))
					{
						log("Target Element is not visible",LogType.ERROR_MESSAGE);		
						Assert.fail("Target Element is not visible");
					}
				}

			}		
			if(IsCheckLeftArrow=="Y")
			{
				getCommand().waitForTargetPresent(RightElement);
				getCommand().mouseHover(RightElement);
				getCommand().click(RightElement);

				getCommand().waitForTargetPresent(LeftElement);
				if(!getCommand().isTargetPresent(LeftElement))
				{
					log("Left arrow is not visible",LogType.ERROR_MESSAGE);		
					Assert.fail("Left arrow is not visible");	
				}
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage CheckCrossElementsFunctionality_OverlayDisplay() 
	{
		try
		{
			log("Checking cross Functionalit in Overlay Display of KohlerIdeas",LogType.STEP);
			getCommand().waitForTargetPresent(OverlayDisplayCross);
			getCommand().mouseHover(OverlayDisplayCross);
			getCommand().click(OverlayDisplayCross);
			getCommand().waitFor(3);
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}		
		return this;
	}

	public Kohler_HomePage CheckShareElementsFunctionality_OverlayDisplay()
	{
		try
		{	
			List<Target> TargetShareElements = new ArrayList<Target>();
			TargetShareElements.add(OverlayDisplay_Share_F);
			TargetShareElements.add(OverlayDisplay_Share_T);
			TargetShareElements.add(OverlayDisplay_Share_P);

			log("Verifying new Window page opens after clicking on each share button",LogType.STEP);	
			for(Target Target:TargetShareElements)
			{			
				String OldWindow = getCommand().getCurrentWindowID();
				getCommand().waitForTargetPresent(Target);
				getCommand().mouseHover(Target);
				getCommand().click(Target);

				//String OldWindow = getCommand().getCurrentWindowID();
				getCommand().waitFor(3);

				WindowHandles();
				
				//getCommand().waitFor(3);
				getCommand().driver.switchTo().window(OldWindow);
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage WindowHandles()
	{
		try
		{
			int Win_size = getCommand().driver.getWindowHandles().size();
			log("Open Windows Size is :" + Win_size ,LogType.STEP);
			ArrayList<String> Windows = new ArrayList<String> (getCommand().driver.getWindowHandles());

			for(int i=0;i<Windows.size();i++)
			{
				getCommand().driver.switchTo().window(Windows.get(i));
				String pageTitle = getCommand().driver.getTitle();
				log("Page title is :"+pageTitle ,LogType.STEP);				
			}
			getCommand().driver.close();
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}

	public Kohler_HomePage JavascripExecutor(WebElement Element, String IsScrollIntoView, String Click)
	{
		try
		{
			if(IsScrollIntoView.equals("Y"))
			{
				JavascriptExecutor js = (JavascriptExecutor) getCommand().getDriver(); 
				js.executeScript("arguments[0].scrollIntoView(true);", Element);
			}

			if(Click.equals("Y"))
			{
				JavascriptExecutor js = (JavascriptExecutor) getCommand().getDriver(); 
				js.executeScript("arguments[0].click();", Element);
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage VerifyDiscoverthePossibilities_LearnMore1()
	{
		try
		{
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Clicking on Lear more CTA available in each carousel slide of 'Discover the Possibilities' grid",LogType.STEP);
			for (WebElement SlickDot : SlickDots)
			{
				
				
				
				
			}
			
			
			
			
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}


	public static final Target leftArrow=new Target("leftArrow","//*[@id='page-content-home']/div[1]/div[1]/button[1]",Target.XPATH);
	public static final Target rightArrow=new Target("rightArrow","//*[@id='page-content-home']/div[1]/div[1]/button[2]",Target.XPATH);
	public static final Target carouselHeader=new Target("carouselHeader","//div[@class='vertical-center-parent']//following::h2[1]",Target.XPATH);
	
	public Kohler_HomePage VerifyHeroArrow()
	{
		boolean status = false;
		
		WebElement element = getCommand().driver.findElement(By.xpath("//*[@class='carousel-hero full-bleed slick-initialized slick-slider']//following-sibling::ul/li/button"));
		JavascriptExecutor executor = (JavascriptExecutor)getCommand().driver;
		executor.executeScript("arguments[0].click();", element);
		
		getCommand().waitForTargetPresent(carouselHeader).mouseHover(carouselHeader);
		
		status = getCommand().isTargetVisible(leftArrow);
		log("Verify that user is able to view the left arrow and status is :"+ status,LogType.STEP);
		Assert.assertTrue(status, "user is able to view the left arrow");
		getCommand().isTargetVisible(rightArrow);
		status = getCommand().isTargetVisible(leftArrow);
		Assert.assertTrue(status, "user is able to view the left arrow");
		log("Verify that user is able to view the left arrow and status is :"+ status,LogType.STEP);
		
//		log("Verify if user is able to view carousel header",LogType.STEP);
//		//getCommand().isTargetVisibleAfterWait(carouselHeader,5);
//		
//		log("Hover on the carosel to hold it and view the side arrows",LogType.STEP);
////		WebElement carouselHeader = getCommand().driver.findElement(By.xpath("//div[@class='vertical-center-parent']//following::h2[1]"));
////		Actions carousel = new Actions(getCommand().driver);
////		carousel.moveToElement(carouselHeader).build().perform();
//		
//		ArrayList<String> caroselList = new ArrayList<String>();
//		// images list
//		List<WebElement> carosels = getCommand().driver.findElements(By.xpath("//div[@class='vertical-center-parent']//following::h2[1]"));
//		
//		//nav dots
//		List<WebElement> navDots = getCommand().driver.findElements(By.xpath("//*[@class='carousel-hero full-bleed slick-initialized slick-slider']//following-sibling::ul/li/button"));
//		
//		
//		for (int i = 0; i < carosels.size(); i++)
//		{
//			String caroselHeader = carosels.get(i).getText();
//			caroselList.add(caroselHeader);
//		}
//		
//		for (int i = 0; i < carosels.size(); i++)
//		{
//			String caroselHeader = carosels.get(i).getText();
//			if(caroselHeader.equalsIgnoreCase(caroselList.get(i)))
//			{
//				navDots.get(i+1).click();
//				Actions carousel = new Actions(getCommand().driver);
//				carousel.moveToElement(carosels.get(i)).build().perform();
//				
////				getCommand().waitForTargetPresent(carouselHeader).mouseHover(carouselHeader);
////				getCommand().waitFor(5);
//				status = getCommand().isTargetVisible(leftArrow);
//				log("Verify that user is able to view the left arrow and status is :"+ status,LogType.STEP);
//				Assert.assertTrue(status, "user is able to view the left arrow");
//				getCommand().isTargetVisible(rightArrow);
//				status = getCommand().isTargetVisible(leftArrow);
//				Assert.assertTrue(status, "user is able to view the left arrow");
//				log("Verify that user is able to view the left arrow and status is :"+ status,LogType.STEP);
//				
//			}
//		}
//		
//		
//		
//		
//		
////		
////		getCommand().waitForTargetPresent(carouselHeader).mouseHover(carouselHeader);
//////		getCommand().isTargetPresent(carouselHeader);
//////		getCommand().mouseHover(carouselHeader);
////		status = getCommand().isTargetVisible(leftArrow);
////		log("Verify that user is able to view the left arrow and status is :"+ status,LogType.STEP);
////		Assert.assertTrue(status, "user is able to view the left arrow");
////		getCommand().isTargetVisible(rightArrow);
////		status = getCommand().isTargetVisible(leftArrow);
////		Assert.assertTrue(status, "user is able to view the left arrow");
////		log("Verify that user is able to view the left arrow and status is :"+ status,LogType.STEP);
////		
//		
		
		
		
		
		return this;
	}

}